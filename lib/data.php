<?php

return array(
    'oorsprong_values' => array('C' => 'Mij', 'E' => 'Deskundige', 'P' => 'Tussenpersoon', 'R' => 'Hersteller'),
    'aanstelling_values' => array('D' => 'BESTEK', 'E' => 'EXPERTISE'),
    'aanwijzing_values' => array('E' => 'Expliciet', 'I' => 'Impliciet'),
    'bezoldigd_deskundige_values' => array('Y' => 'ja', 'N' => 'neen'),
    'inlichtingen_maatschappij_vrijstelling_aard_values' => array('1' => 'bedrag', '2' => '% van de verzekerde waarde', '3' => '% van de waarde van het schadegeval', '5' => 'minimum + % van de waarde van het schadegeval'),
    'inlichtingen_maatschappij_grensbedrag_direkte_betaling_aard_values' => array('E' => 'Expliciet', 'I' => 'Impliciet', '' => 'blanco'),
    'inlichtingen_maatschappij_grensbedrag_direkte_betaling_laatste_wijziging_door_values' => array('C' => 'de Mij', 'E' => 'de Expert', 'P' => 'de tussenpersoon', '' => 'blanco'),
    'voertuig_buiten_gebruik_values' => array('N' => 'niet buiten gebruik', 'Y' => 'buiten gebruik bij de hersteller', 'A' => 'elders buiten gebruik'),
    'benzine_values' => array('E' => 'benzine', 'D' => 'diesel', 'G' => 'LPG', 'H' => 'hybride', 'Z' => 'elektrisch', 'A' => 'ander'),
    'verzender_values' => array('1' => 'mij', '2' => 'deskundige', '3' => 'hersteller', '4' => 'tussenpersoon'),
    'historiek_van_de_verzendingen_values' => array('' => 'is niet vermeld', 'Y' => '	is volledig : hij begint vanaf de aanstelling', 'N' => 'onvolledig'),
    'statuut_van_de_expertise_values' => array('' => '	klassieke expertise',
        '1' => 'nieuwe circuit expertise bij een normale deskundige',
        '3' => 'nieuwe circuit expertise bij een gewone deskundige',
        '2' => 'controle-expertise van de hersteller',
        '4' => 'controle-expertise van de deskundige',
        'S' => 'Smart expertise : Nieuw',
        'E' => 'Smart expertise : Verstreken',
        'W' => 'Smart expertise : In analyse',
        'T' => 'Smart expertise : Verwerkt',
    ),
    'auto_process_values' => array('A' => 'Aanvaarding', 'I' => 'Aanvaarding expert opdracht', 'V' => 'Aanvaarding expert aangesteld', 'R' => 'Expert op afstand', 'P' => 'Expert ter plaatse'),
    'onmiddellijke_terugzending'    =>  array('Y' => 'ja', 'N' => 'neen'),
    'tussenkomst_type_values' => array('C' => 'Maatschappij', 'E' => 'Deskundige','R' => 'Hersteller','I' => 'Tussenpersoon'),
    'bemiddelaar_in_de_opdracht' => array('A' => 'Opdrachtgever', 'B' => 'Deskundige','C' => 'Hersteller','D' => 'Tussenpersoon','T'=>'Derde','E'=>'Tegenpartij','F'=>'2de deskundige','G'=>'3de deskundige'),
    'uitslag_values' => array('P' => 'totaal verlies', '1' => '1ste bedrag','2' => '2de bedrag','F' => 'Forfait','A'=>'Afstand','X'=>'absolute forfait'),
    'totaal_verlies_values'    =>  array(
        ' '=>'niet vermeld',
        '1' => 'economisch', 
        'W' => 'voertuig waarvan de meerderheid van de structuur vervormd is',
        'X'=>'voertuig in verschillende stukken','Y'=>'voertuig vernield door brand',
        'Z'=>'voertuig dat een bepaalde tijd in het water heeft verbleven',
        'R'=>'ingetrokken'
        ),
    'aard_1ste_bedrag_values'    =>  array('A' => 'uitslag van een AUDATEX berekening', 'F' => 'gedwongen'),
    'VIN_berekening_values'    =>  array('0' => 'de opdracht bevat geen chassisnummer of de productie van de Audatex codes is niet mogelijk voor het vermelde chassisnummer', '1' => 'de laatste Audatex berekening heeft de Audatex codes, geproduceerd door de VIN, gebruikt,','2'=>'de laatste Audatex berekening heeft de Audatex codes, geproduceerd door de VIN, niet gebruikt, maar dit is mogelijk'),
    'technische_controle_values'    =>  array(''=>'indien niet vermeld','A1'=>'chassis','A2'=>'stuurinrichting','A3'=>'ophanging','A4'=>'reminrichting',
        'B1'=>'chassis','B2'=>'stuurinrichting','B3'=>'ophanging','B4'=>'reminrichting','R'=>'ingetrokken','N'=>'geen technische controle'
        ),
    'sleping_values'    =>  array('Y' => 'ja', 'N' => 'neen'),
    'honoraria_aard_tarificatie_values'    =>  array('I' => 'Impliciet', 'E' => 'Expliciet'),
    'statuut_van_direkte_betaling_van_een_expertise_values'    =>  array(
        ' ' => 'nog niet afgesloten expertise', 
        '0' => 'tussenkomst hoger dan grensbedrag of grensbedrag niet vermeld',
        '2' => 'tussenkomst nihil',
        '4' => 'de hersteller is niet vermeld, aangesloten (of erkend indien Maatschappij erkend)',
        '5' => 'de gewenste uitslag is noch 1, noch F',
        '6' => 'de AUDATEX berekening werd niet van het bestek overgenomen',
        '7' => 'het grensbedrag is expliciet nihil',
        'Y' => 'de direkte betaling is toegekend',
    ),
    
    'bezoldigd_deskundige_values' => array(' '=>'niet aangeduid','Y' => 'ja', 'N' => 'neen'),
    'als_het_wrak_niet_verkocht_werd_aan_de_beste_offerte_van_het_journaal_bemiddelaar_code_values'    =>  array(
        'A' => 'Een collega van wie de lijst van offertes niet werd overgenomen en die een aanvraag van offertes heeft gelanceerd, hetgeen verklaart : -waarom die wagen niet in het journaal werd geplaatst -	of waarom hij is verkocht geweest aan een andere offerte dan de beste van het journaal',
        'B' => 'vergissing bij ingave van de bemiddelaar XXX XXX XXX de bemiddelaar is : ofwel de wrakopkoper welke een foutieve ingave heeft gedaan op de juiste offerte dat hij heeft gekregen van de wrakker',
        'C' => 'geldigheid van de overschrede offerte',
        'D' => 'bijkomend ongeval',
        'E' => 'de verkoopstelsel (Marge/geen Marge) werd gewijzigd na het afsluiten van de aanvraag van de offertes',
        'F' => 'het wrak heeft minder waarde dan het bedrag doorgegeven door de maatschappij',
    ),
    'aanmerking_aanvraag_wrakoffertes_values'    =>  array(
        '01' => 'GEWIJZIGD CHASSISNUMMER',
        '02' => 'VOERTUIG NOOIT INGESCHREVEN (MET 705)',
        '03' => 'LPG VERWIJDERD',
        '04' => 'SPECIALE VELGEN VERWIJDERD',
        '05' => 'HARDTOP VERWIJDERD',
        '06' => 'SPOILER VERWIJDERD',
        '07' => 'AIR CONDITIONING VERWIJDERD',
        '08' => 'RADIO VERWIJDERD',
        '09' => 'CARROSSERIE VERWIJDERD',
        '10' => 'LAADBRUG VERWIJDERD',
        '11' => 'HIJSKRAAN VERWIJDERD',
        '12' => 'KOELGROEP VERWIJDERD',
        '13' => 'GSM VERWIJDERD',
        '14' => 'TREKHAAK VERWIJDERD',
        '15' => 'GEEN STAANGELD',
        '16' => 'GEEN AFHAALKOSTEN',
        '17' => 'GPS VERWIJDERD',
    ),
    'Conclusie_van_de_hersteller_values'    =>  array(
        '1' => 'de berekening is conform met de voorziene herstelling',
        '2' => 'het voertuig is waarschijnlijk een totaal verlies',
        '3' => 'het voertuig heeft meerdere schadezones van verschillende oorsprong',
        '4' => 'de berekening stemt overeen maar een technisch advies van de deskundige is noodzakelijk',
        '5' => 'de berekening stemt overeen maar het voertuig vereist een technische controle',
        '6' => 'door de omvang van de schade is een deskundige ter plaatse vereist',
    ),
    
    
    
    
);


$line_data[1]['aanstelling']                = $this->data['aanstelling_values'][$line_data[1]['aanstelling']];
                    $line_data[1]['oorsprong']                  = $this->data['oorsprong_values'][$line_data[1]['oorsprong']];
                    $line_data[2]['aanwijzing_deskundige']     = $this->data['aanwijzing_values'][$line_data[2]['aanwijzing_deskundige']];
                    $line_data[2]['bezoldigd_deskundige']      = $this->data['bezoldigd_deskundige_values'][$line_data[2]['bezoldigd_deskundige']];
                    $line_data[3]['inlichtingen_maatschappij_vrijstelling_aard']  = $this->data['inlichtingen_maatschappij_vrijstelling_aard_values'][$line_data[3]['inlichtingen_maatschappij_vrijstelling_aard']];
                    $line_data[3]['inlichtingen_maatschappij_grensbedrag_direkte_betaling_aard']   = $this->data['inlichtingen_maatschappij_grensbedrag_direkte_betaling_aard_values'][$line_data[3]['inlichtingen_maatschappij_grensbedrag_direkte_betaling_aard']];
                    $line_data[3]['inlichtingen_maatschappij_grensbedrag_direkte_betaling_laatste_wijziging_door']  = $this->inlichtingen_maatschappij_grensbedrag_direkte_betaling_laatste_wijziging_door_values[3][$line_data['inlichtingen_maatschappij_grensbedrag_direkte_betaling_laatste_wijziging_door']];
                    $line_data[8]['voertuig_buiten_gebruik']        = $this->data['voertuig_buiten_gebruik_values'][$line_data[8]['voertuig_buiten_gebruik']];
                    $line_data[8]['benzine']                        = $this->data['benzine_values'][$line_data[8]['benzine']];
                    $line_data[10]['verzender']                     = $this->data['verzender_values'][$line_data[10]['verzender']];
                    $line_data[10]['Volgende_toevoegels_historiek_van_de_verzendingen']  = $this->data['historiek_van_de_verzendingen_values'][$line_data[10]['Volgende_toevoegels_historiek_van_de_verzendingen']];
                    $line_data[12]['brandstof']                     = $this->data['benzine_values'][$line_data[12]['brandstof']];
                    $line_data[13]['statuut_van_de_expertise']      = $this->data['statuut_van_de_expertise_values'][$line_data[13]['statuut_van_de_expertise']];
                    $line_data[13]['Auto_process']                  = $this->data['auto_process_values'][$line_data[13]['Auto_process']];
                    $line_data[18]['Was_er_een_onmiddellijke_terugzending']                  = $this->data['onmiddellijke_terugzending'][$line_data[18]['Was_er_een_onmiddellijke_terugzending']];
                    $line_data[18]['Verantwoordelijke_van_de_tussenkomst_type']                  = $this->data['tussenkomst_type_values'][$line_data[18]['Verantwoordelijke_van_de_tussenkomst_type']];
                    $line_data[18]['Verantwoordelijke_van_de_tussenkomst_nummer_bemiddelaar_in_de_opdracht']    = $this->data['bemiddelaar_in_de_opdracht'][$line_data[18]['Verantwoordelijke_van_de_tussenkomst_nummer_bemiddelaar_in_de_opdracht']];                
                    $line_data[21]['uitslag']                  = $this->data['uitslag_values'][$line_data[21]['uitslag']];
                    $line_data[21]['totaal_verlies_aard_totaal_verlies']                  = $this->data['totaal_verlies_values'][$line_data[21]['totaal_verlies_aard_totaal_verlies']];
                    $line_data[22]['aard_1ste_bedrag']                  = $this->data['aard_1ste_bedrag_values'][$line_data[22]['aard_1ste_bedrag']];
                    $line_data[22]['VIN_berekening']                  = $this->data['VIN_berekening_values'][$line_data[22]['VIN_berekening']];
                    $line_data[23]['technische_controle']                  = $this->data['technische_controle_values'][$line_data[23]['technische_controle']];
                    $line_data[23]['technische_controle']                  = $this->data['technische_controle_values'][$line_data[23]['technische_controle']];
                    $line_data[23]['sleping']                  = $this->data['sleping_values'][$line_data[23]['sleping']];
                    $line_data[23]['afzonderlijke_voorlopige_herstelling']                  = $this->data['sleping_values'][$line_data[23]['afzonderlijke_voorlopige_herstelling']];
                    $line_data[23]['opdracht_afgesloten']                  = $this->data['sleping_values'][$line_data[23]['opdracht_afgesloten']];
                    $line_data[27]['honoraria_aard_tarificatie']                  = $this->data['honoraria_aard_tarificatie_values'][$line_data[27]['honoraria_aard_tarificatie']];
                    $line_data[27]['transit_van_honoraria']                  = $this->data['sleping_values'][$line_data[27]['transit_van_honoraria']];
                    $line_data[28]['statuut_van_direkte_betaling_van_een_expertise']                  = $this->data['statuut_van_direkte_betaling_van_een_expertise_values'][$line_data[28]['statuut_van_direkte_betaling_van_een_expertise']];
                    
                    
                    $line_data[29]['vervallen_aanvraag_van_offertes']                  = $this->data['sleping_values'][$line_data[29]['vervallen_aanvraag_van_offertes']];
                    $line_data[29]['geschrapte_aanvraag_van_offertes']                  = $this->data['sleping_values'][$line_data[29]['geschrapte_aanvraag_van_offertes']];
                    $line_data[29]['werd_de_deskundige_door_de_hersteller_vereist']                  = $this->data['sleping_values'][$line_data[29]['werd_de_deskundige_door_de_hersteller_vereist']];
                    $line_data[29]['toekomstige_bijvullingen_voertuig_verkocht_op_de_marge']                  = $this->data['sleping_values'][$line_data[29]['toekomstige_bijvullingen_voertuig_verkocht_op_de_marge']];
                    $line_data[29]['als_het_wrak_niet_verkocht_werd_aan_de_beste_offerte_van_het_journaal_bemiddelaar_code']                  = $this->data['als_het_wrak_niet_verkocht_werd_aan_de_beste_offerte_van_het_journaal_bemiddelaar_code_values'][$line_data[29]['als_het_wrak_niet_verkocht_werd_aan_de_beste_offerte_van_het_journaal_bemiddelaar_code']];
                    $line_data[29]['aanmerking_aanvraag_wrakoffertes']                  = $this->data['aanmerking_aanvraag_wrakoffertes_values'][$line_data[29]['aanmerking_aanvraag_wrakoffertes']];
                    
                    $line_data[29]['Conclusie_van_de_hersteller']                  = $this->data['Conclusie_van_de_hersteller_values'][$line_data[29]['Conclusie_van_de_hersteller']];