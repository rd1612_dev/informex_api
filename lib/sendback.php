<?php
$objJob = Job::load(1);

$leasing	= EntityJob::LoadByJobAndEntityType($objJob->Id, EntityJob::ROLE_LEASECOMPANY);
$broker		= EntityJob::LoadByJobAndEntityType($objJob->Id, EntityJob::ROLE_BROKER);
$harmner	= EntityJob::LoadByJobAndEntityType($objJob->Id, EntityJob::ROLE_HARMNER);
$repairer	= EntityJob::LoadByJobAndEntityType($objJob->Id, EntityJob::ROLE_REPAIRER);

// TYRES
$VL	= VehicleTires::querySingle(QCubed\Query\QQ::andCondition(
			QCubed\Query\QQ::like(QQN::VehicleTires()->VehicleId, $objJob->VehicleId),
			QCubed\Query\QQ::like(QQN::VehicleTires()->Type, "vl")

		));
$VR	= VehicleTires::querySingle(QCubed\Query\QQ::andCondition(
			QCubed\Query\QQ::like(QQN::VehicleTires()->VehicleId, $objJob->VehicleId),
			QCubed\Query\QQ::like(QQN::VehicleTires()->Type, "vr")

		));
$AL	= VehicleTires::querySingle(QCubed\Query\QQ::andCondition(
			QCubed\Query\QQ::like(QQN::VehicleTires()->VehicleId, $objJob->VehicleId),
			QCubed\Query\QQ::like(QQN::VehicleTires()->Type, "al")

		));
$AR	= VehicleTires::querySingle(QCubed\Query\QQ::andCondition(
			QCubed\Query\QQ::like(QQN::VehicleTires()->VehicleId, $objJob->VehicleId),
			QCubed\Query\QQ::like(QQN::VehicleTires()->Type, "ar")

		));
$Reserve	= VehicleTires::querySingle(QCubed\Query\QQ::andCondition(
			QCubed\Query\QQ::like(QQN::VehicleTires()->VehicleId, $objJob->VehicleId),
			QCubed\Query\QQ::like(QQN::VehicleTires()->Type, "spare")

		));
array(
	
	
//	"jobnumber"					=> $objJob->Id, 
	// jobaccidentdate
	"24"			=> $objJob ? $objJob->AccidentDate->format("d-m-Y") : "", 
	// mandatetype
	"9B"			=> $objJob->JobDetail->MandateType ? $objJob->JobDetail->MandateType : "",
//	"jobpolicynr"				=> $objJob->JobDetail ? (string)$objJob->JobDetail->PolicyNumber : "", 
//	"jobpolicytype"				=> (isset($objJob->JobDetail->PolicyType)) ? $objJob->JobDetail->PolicyType->{\QCubed\Project\Application::$LocaleName} : "", 
//	"jobtechnicalcheckup"		=> "", 
//	"jobdateexpertise"			=> "", 
//	"jobexpertassuralia"		=> ($objJob->ExpertId) ? $objJob->Expert->Assuralia : '',
//	"jobexpertinstitutionnumber"=> ($objJob->ExpertId) ? $objJob->Expert->Institute : '',
	// damagedescription
	"Z5"			=> $objJob->Damage ? $objJob->Damage->Description : "",
	// damagenumber
	"23"	=> $objJob->JobDetail->DamageClaimNumber ? $objJob->JobDetail->DamageClaimNumber : "",
//	"damagefirstimpactpoint"	=> $objJob->Damage ? $objJob->Damage->FirstImpactPoint : "",
//	"damagehitdirection"		=> $objJob->Damage ? $objJob->Damage->HitDirection : "",
//	"damageexcess"				=> $objJob->Damage ? $objJob->Damage->Excess : "",
//	"damageexcesstype"			=>  $objJob->Damage ? $objJob->Damage->ExcessType : "",
//	"damagecause"				=>  $objJob->Damage ? $objJob->Damage->Cause : "",
//	"damagemilage"				=>  $objJob->Damage ? $objJob->Damage->Milage : "",
//	"damagefault"				=>  $objJob->Damage ? $objJob->Damage->Fault : "",
	//  damagedamagepoints
	"G1"		=>  $objJob->Damage ? $objJob->Damage->DamagePoints : "",
//	"damagedateatrepairer"		=>  $objJob->Damage ? $objJob->Damage->DateAtRepairer->format("Y-m-d") : "",
//	"damagetowed"				=>  $objJob->Damage ? $objJob->Damage->Towed : "",
//	"damagebreakdowncosts"			=>  $objJob->Damage ? $objJob->Damage->BreakDownCosts : "",
//	"damageprovisionalrepair"		=>  $objJob->Damage ? $objJob->Damage->ProvisionalRepair : "",
//	"damageprovisionalrepaircosts"	=> $objJob->Damage ? $objJob->Damage->ProvisionalRepairCosts : "",
	// damagemin
	"Y1"					=> $objJob->Damage ? $objJob->Damage->Min : "",
	// damagemaw
	"Y2"					=> $objJob->Damage ? $objJob->Damage->Max : "",
	// damagevalue
	"26"				=> $objJob->Damage ? $objJob->Damage->Value : "",
//	"damagelimit"				=> $objJob->Damage ? $objJob->Damage->Limit : "",
//	"damageamountlimit"			=> $objJob->Damage ? $objJob->Damage->AmountLimit : "",
//	"damagelimitrdr"			=>$objJob->Damage ? $objJob->Damage->LimitRdr : "",
//	"damagetodeduct"			=> $objJob->Damage ? $objJob->Damage->ToDeduct : "",
//	"damagetotalloss"			=> $objJob->Damage ? $objJob->Damage->TotalLoss : "",
//	"damageimmobilised"			=> $objJob->Damage ? $objJob->Damage->Immobilised : "",
//
//	"damagetotallosstypename"	=> $objJob->Damage ? $objJob->Damage->TotalLossTypeName: "",
//	"damagetotallosstypecode"	=> $objJob->Damage ? $objJob->Damage->TotalLossTypeCode: "",
//	"damagemargeonsale"			=> $objJob->Damage ? $objJob->Damage->MargeOnSale: "",
//	"damagedestinationvehicle"	=> $objJob->Damage ? $objJob->Damage->DestinationVehicle: "",
//	"damagedeductedvalue"		=> $objJob->Damage ? $objJob->Damage->DeductedValue: "",
//	"damagesubjectedtovat"		=> $objJob->Damage ? $objJob->Damage->SubjectedToVAT: "",
//	"damagenosubjectedtovat"	=> $objJob->Damage ? $objJob->Damage->NoSubjectedToVAT: "",
//	"damagetransferaccessories"	=> $objJob->Damage ? $objJob->Damage->TransferAccessories: "",
//	"damagereplaceaccessories"	=> $objJob->Damage ? $objJob->Damage->ReplaceAccessories: "",
			
	// DESKUNDIGE START 		
	// grondig mandaat
	"4D"			=> $objJob->JobDetail->ThoroughMandate ? $objJob->JobDetail->ThoroughMandate : "",
	// 	expertise op afstand
	"7G"			=> $objJob->JobDetail->RemoteInspection ? $objJob->JobDetail->RemoteInspection : "",
	// type van aanstelling
	"12"			=> $objJob->JobDetail->AppointmentType ? $objJob->JobDetail->AppointmentType : "",
	
	// nummer van de deskundige
	"14"			=> $objJob->Expert->Informex ? $objJob->Expert->Informex : "",
	// naam
	"16"			=> $objJob->Expert->User->FirstName ? $objJob->Expert->User->FirstName . $objJob->Expert->User->LastName : "",
	// goedkeuring ASSURALIA
	"15"			=> $objJob->Expert->Assuralia ? $objJob->Expert->Assuralia  : "",
	// IAE nummer
	"6I"			=> $objJob->Expert->Institute ? $objJob->Expert->Institute : "",
	// streekcode
	"13"			=> "",
	// dossiernummer
	"E4"			=> $objJob->InformexNumber ? $objJob->InformexNumber : "",
	// datum aanvang beheer
	"3D"			=> $objJob->Created ? $objJob->Created : "",
			
	/*
		 2de Deskundige	
		-	type van mandaat	0C/--
		VP	:	cfr : aard van mandaat, expert

		-	nummer	F3/-(5)-
		VP	:	moet bestaan in het bestand van deskundigen (WE1)

		-	naam	F5/---(10)---
		VP	:	willekeurig

		-	goedkeuring	F4/----
		VP	:	elk nummer van goedkeuring

		-	dossiernummer	F7/----(14)----
		VP	:	willekeurig
	*/
	/*
		3de Deskundige

		-	type van mandaat	1C/--
		VP	:	cfr : aard van mandaat, expert

		-	nummer	W0/-----
		VP	:	moet bestaan in het bestand van deskundigen (WE1)

		-	naam	W2/---(10)---
		VP	:	willekeurig

		-	goedkeuring	W1/----
		VP	:	elk nummer van goedkeuring

		-	dossiernummer	W4/-----(14)-----
		VP	:	willekeurig
	 */
			
	// START LASTGEVER
	// nummer van
	"19"			=> "",
	// nummer CBFA
	"20"			=> "",
	// polis
	"22"			=> $objJob->JobDetail ? $objJob->JobDetail->PolicyNumber : "",
	// schadegeval nummer
	"23"			=> $objJob->JobDetail ? $objJob->JobDetail->DamageClaimNumber : "",
	// 	datum schadegeval
	"24"			=> $objJob->AccidentDate ? $objJob->AccidentDate : "",
	// verzekerde waarde
	"25"			=> $objJob->JobDetail->ValueInsurance ? $objJob->JobDetail : "",
	// vrijstelling
			// aard
			"G2"	=> $objJob->JobDetail ? $objJob->JobDetail->TypeExemption : "",
			// waarde
			"26"	=> $objJob->JobDetail ? $objJob->JobDetail->Exemption : "",
			// minimum
			"Y1"	=> $objJob->Damage->Min ? $objJob->Damage->Min : "",
			// maximum
			"Y2"	=> $objJob->Damage->Max ? $objJob->Damage->Max : "",
			//  af te trekken van de tussenkomst?
			"59"	=> $objJob->Damage->ToDeduct ? $objJob->Damage->ToDeduct : "",
	// grensbedrag direkte betaling
			// type
			"27"	=> $objJob->JobDetail ? $objJob->JobDetail->AppointmentType : "",
			// waarde
			"28"	=> "",
			// grensbedrag RDR
			"O9"	=> $objJob->Damage->LimitRdr ? $objJob->Damage->LimitRdr : "",
			// waarde derving
			"87"	=> $objJob->JobBids ? $objJob->JobBids->CostsLoss : "",
			// referentie bij de creatie
			"0G"	=> $objJob->Id ? $objJob->Id : "",
			
	/*
		Mij tegenpartij
		-	nummer van	29/----
		VP	:	moet bestaan in het bestand van de partij maatschappijen (W'C6)

		-	nummer CBFA	74/----
		VP	:	4 cijfers
		VD	:	het CBFA nummer verbonden aan het nummer van de Mij tegenpartij

		-	polis	30/----(14)------
		VP	:	willekeurig

		-	schadegevalnummer	31/----(14)------
		VP	:	willekeurig
	 */		
	
	// Leaser
	//	-	nummer van	9C/----
	"9C"		=> $leasing->Entity->CompanyNr ? $leasing->Entity->CompanyNr : "",
	//	-	dossiernummer	1D/-(14)
	"1D"		=> $leasing->Reference ? $leasing->Reference : "",
	//	-	honoraria ten laste van de leaser ?	2D/-
	//	VP	:	Y, N
	//	Gedwongen naar N wanneer de leaser onbestaand of niet aangesloten is.

	 
	// Makelaar
	//	   -	nummer van	32/-(5)-
	"32"		=> $broker->Entity ? $broker->Entity->CompanyNr : "",
	//	   -	dossiernummer	E5/----(14)------
	"E5"		=> $broker->Reference ? $broker->Reference : "",
	//	   -	naam	35/----(30)-------
	"35"		=> $broker->Entity ? $broker->Entity->Name : "",
	//	   -	straat	37/----(30)-------
	"37"		=> $broker->Entity->Address ? $broker->Entity->Address->Street : "",
	//	   -	postcode	39/----
	"39"		=> $broker->Entity->Address ? $broker->Entity->Address->City->PostalCode : "",
	//	   -	gemeente	40/----(25)-------
	"40"		=> $broker->Entity->Address ? $broker->Entity->Address->City->Name : "",
	//	   -	telefoonnummer	W5/---(10)---
	"W5"		=> $broker->Entity->EntityPhone ? $broker->Entity->EntityPhone->Nr : "",
			
	//	Schadelijder
	//	-	naam	42/----(30)-------
	"42"		=> $harmner ? $harmner->Entity->Name : "",
	//	-	straat	44/----(30)-------
	"44"		=> $harmner->Entity->Address ? $harmner->Entity->Address->Street : "",
	//	-	postcode	46/----
	"46"		=> $harmner->Entity->Address ? $harmner->Entity->Address->City->PostalCode : "",
	//	-	gemeente	47/----(25)-------
	"46"		=> $harmner->Entity->Address ? $harmner->Entity->Address->City->Name : "",
	//	-	telefoonnummer	50/---(10)---
	"50"		=> $harmner->Entity->EntityPhone ? $harmner->Entity->EntityPhone->Nr : "",
	//	-	tweede telefoonnummer	1G/---(10)---
	//
	//	-	e-mail adres	2G/----(70)----
	"2G"		=> $harmner->Entity->EntityEmail ? str_replace('@','^',$harmner->Entity->EntityEmail->Address) : "",
	//	Remarque	:	het karakter @ moet vervangen worden door het circonflexe (^) (dakvormig accent)	
	//	-	beroep	52/----(15)-------
	"52"		=> $harmner->Entity ? $harmner->Entity->Profession : "", 
	//	-	taal	49/-
	"49"		=> $harmner->Entity ? $harmner->Entity->Language : "",
	//	-	terugvorderbaar ged. BTW (%)	51/---
	"51"		=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->PercentageRecoveredVat : "",
	//	-	BTW 6%	E3/-
	"E3"		=> "",
	
	
//		Hersteller
//		-	nummer van	53/---(9)---
	"53"		=> $repairer->Entity ? $repairer->Entity->CompanyNr: "",
//		-	nummer van bestek	E6/---(14)-------
//		VP	:	willekeurig
//		VC	:	dossiernummer indien de bemiddelaar een hersteller is
//		VD	:	bestek nummer van de opdracht overgenomen bestek
//
//		-	naam	78/----(30)-------
	"78"		=> $repairer->Entity ? $repairer->Entity->Name : "",
//		-	straat	56/----(30)-------
	"56"		=> $repairer->Entity->Address ? $repairer->Entity->Address->Street : "",
//		-	postcode	80/----
	"80"		=> $repairer->Entity->Address ? $repairer->Entity->Address->City->PostalCode : "",
//		-	plaats	81/----(25)-------
	"81"		=> $repairer->Entity->Address ? $repairer->Entity->Address->City->Name : "",
//		-	telefoonnummer	Y3/---(10)---
	"Y3"		=> $repairer->Entity->EntityPhone ? $repairer->Entity->EntityPhone->Nr : "",
//		-	depotnummer	2I/---(9)---
	"2I"		=> $repairer->Entity ? $repairer->Entity->CompanyNr: "",
	
//Voertuig
	//-	merk	G3/----(20)------
	"G3"			=> $objJob->Vehicle ? $objJob->Vehicle->Make->Name : "",
	//-	model	75/----(15)------
	"75"			=> $objJob->Vehicle ? $objJob->Vehicle->Model->Name : "",
	//-	plaat	G0/---(10)---
	"G0"			=> $objJob->Vehicle ? $objJob->Vehicle->Plate : '',
	//-	chassis	60/-----(17)-----
	"60"			=> $objJob->Vehicle ? $objJob->Vehicle->Vin : '',
	//-	kilometerstand	Y5/-(7)-
	"Y5"			=> $objJob->Vehicle ? $objJob->Vehicle->Milage : 'N', 
	//-	vermogen (kw)	77/----
	"77"				=> $objJob->Vehicle ? $objJob->Vehicle->Kw : '', 
	//-	inhoud
	//-	cm³	63/-(5)-
	"63" => $objJob->Vehicle ? $objJob->Vehicle->EngineCapacity : '', 
	//-	laadvermogen (ton)	64/-(5)-
	"64"		=> $objJob->Vehicle ? $objJob->Vehicle->Payload : '', 
	//-	tarra	Y6/-(5)-
	"Y6"			=> $objJob->Vehicle ? $objJob->Vehicle->Tarra : '', 
	//-	aantal plaatsen	65/----
	"65"	=> $objJob->Vehicle ? $objJob->Vehicle->NumberSeats : '', 
	//-	datum eerste in omloopbrenging	67/-(6)-
	"67"	=> $objJob->Vehicle->FirstRegistrationDate ?  $objJob->Vehicle->FirstRegistrationDate->format('d-m-Y') : 'N' , 
	//VP	:	elke samenhangende DDMMJJ datum DD mag 00 zijn, DDMM mag 0000 zijn 
	//-	datum laatste in omloopbrenging	I9/-(6)-
	"I9"	=> $objJob->Vehicle->LastRegistrationDate ?  $objJob->Vehicle->LastRegistrationDate->format('d-m-Y') : '0000',
	//-	Schadezone	G1/---(10)---
	"G1"		=>  $objJob->Damage ? str_replace(',','',$objJob->Damage->DamagePoints) : "",
	//-	voorgaande schade	0A/-
	"0A"		=> $objJob->Damage->PreviousDamage ? "Y" : "N",
	//-	inbraak	7B/-
	"7B"		=> $objJob->Damage->Burglary ? "Y" : "N",
	//-	Voertuig buiten gebruik	68/-
	"68"		=> $objJob->Damage->Immobilised ? $objJob->Damage->Immobilised : "",
	//-	bij hersteller vanaf	1A/-(6)-
	"1A"		=> $objJob->Damage->DateAtRepairer ? $objJob->Damage->DateAtRepairer : "",
	//-	brandstof 
	"2A"		=> $objJob->Vehicle ? $objJob->Vehicle->FeulType : '', 
	//-	Roetfilter	3I/-
	"3I"		=> $objJob->Vehicle ? $objJob->Vehicle->SootFilter : '', 
	//-	CO2-uitstoot (gr/km)	4I/----
	"4I"		=> $objJob->Vehicle ? $objJob->Vehicle->Co2 : '', 
	//-	diefstalbeveiliging	3A/-
	"3A"		=> $objJob->Vehicle->Alarm ? "Y" : "N",
	//Aanmerking aanstelling	
	//-	Lijn 1	Q5/----(75)-----
	//-	Lijn 2	R0/----(75)-----
	//-	Lijn 3	R5/----(75)-----
	//-	Lijn 4	S0/----(75)-----

			
//	expertise
//	Datum	E9/-(6)-
//	VP	:	elke samenhangende DDMMJJ datum
//	VD	:	datum van eerste tussenkomst expertise
//
//	Taal	85/-
//	VP	:	N (Nederlands), F (Frans) (W55)
//	VC	:	-	de taal van de bemiddelaar indien bestek
//	-	de taal van de deskundige indien expertise
//
//	Herstellingsduur (in dagen)	95/----
"95"			=> $objJob->Damage ? $objJob->Damage->RepairLength : "",
//	Gewenst besluit	86/-
"86"			=> $objJob->Damage->TotalLoss ? $objJob->Damage->TotalLoss : '',
//	Bedrag van de 1ste schatting	90/------,--
//	VP	:	elke positieve numerieke waarde of nul (W61)
//	VD	:	bij een AUDATEX berekening : het resultaat van de berekening
//
//	Bedrag van de 2de schatting	91/------,--
//	VP	:	1.	Elke positieve numerieke waarde of nul = bedrag 2de schatting
//	2.	Elke negatieve numerieke waarde = bedrag af te trekken van de 1ste schatting (of van het forfait) om het bedrag van de 2de schatting te bekomen
//	VD	:	0
//
//	Bedrag van het forfait	92/------,--
"92"			=> $objJob->Damage  ? $objJob->Damage->Limit : "0",
//	Bedrag af te trekken van de direkte betaling	73/------,--
"73"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->Deduction : "0",
//	Totaal verlies
//	-	verkoop op de marge	4B/-
"4B"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->Margin : "N",
//	-	Type	F9/-
"F9"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->TotalLossType->Code : "", 
//	-	Marktwaarde	88/------,--
"88"			=> $objJob->Vehicle ? $objJob->Vehicle->MarketValue : '0', 
//	-	Aftrek	H0/------,--
//	VP	:	elke positieve waarde of nul (W61)
//	VD	:	0
//
//	-	Voertuig
//	-	bestemming	H1/-
"H1"			=> $objJob->Damage->DamageDetails->DestinationVehicle ? $objJob->Damage->DamageDetails->DestinationVehicleId : '',
//	-	waarde	E1/------,--
//	VP	:	elke positieve waarde of nul (W61)
//	VD	:	op de vervaldag van de aanvraag van offertes : waarde van de beste offerte
//
//	-	Overplaatsing accessoires	H2/------,--
"H2"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->TransferAccessories : "0",
//	-	Vervanging accessoires	H3/------,--
"H3"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->ReplacementAccessories : "0",
//	-	Diverse onderworpen aan BTW	F0/------,--
"F0"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->SubjectToVat : "0",
//	-	Diverse niet onderworpen aan BTW	6D/------,--
"6D"			=> $objJob->Damage->DamageDetails ? $objJob->Damage->DamageDetails->NoSubjectToVat : "0",
//	-	Offerte
//	-	geldigheid	H4/-(6)-
//	VP	:	elke mogelijke datum DDMMJJ (W) 
//
//	-	geform.	Y7/-(6)-
//	VP	:	elke mogelijke datum DDMMJJ (W) 
//
//	-	als het wrak niet wordt verkocht aan de beste offerte
//	-	code	5B/-
"5B"			=> "",
//	-	bemiddelaarnummer	6B/---(9)---
//
//	De 2 rubrieken zijn voor een bemiddelaar deskundige toegankelijk (WE8)
//
//	-	koper
//	-	nummer	H5/---(9)---
//	VP	:	elk BTW nummer hersteller (WR1)
//	-	dat bestaat
//	-	of gewoon samenhorig is (9 cijfers waarvan de 2 laatste het verschil zijn tot 97 van modulo 97 van de 7 eerste) indien naam, straat, postcode, plaats, meegedeeld worden
//	het teken - in eerste positie; laat toe dat met het nummer, de naam, de straat, postcode, gemeente en telefoon van de koper opnieuw in blancio kan zetten;
//
//	-	naam	H6/-----(30)-----
//	VP	:	willekeurig
//	Waarde indien de zone blanco is en het nummer van de koper bestaat, overeenkomstige zones van de identificatie.
//	VD	:	op de vervaldag van de aanvraag van offertes : de inschrijver van de beste offerte
//
//	-	straat	H8/-----(30)-----
//	VP	:	willekeurig
//	Waarde indien de zone blanco is en het nummer van de koper bestaat, overeenkomstige zones van de identificatie
//	VD	:	op de vervaldag van de aanvraag van offertes : de inschrijver van de beste offerte
//
//	-	postcode	I0/----
//	VP	:	willekeurig, numeriek
//	Waarde indien de zone blanco is en het nummer van de koper bestaat, overeenkomstige zones van de identificatie
//	VD	:	op de vervaldag van de aanvraag van offertes : de inschrijver van de beste offerte
//
//	-	gemeente	I1/-----(25)-----
//	VP	:	willekeurig
//	Waarde indien de zone blanco is en het nummer van de koper bestaat, overeenkomstige zones van de identificatie
//	VD	:	op de vervaldag van de aanvraag van offertes : de inschrijver van de beste offerte
//
//	-	telefoonnummer	Y4/---(10)---
//	Waarde indien de zone blanco is en het nummer van de koper bestaat, overeenkomstige zones van de identificatie
//	VD	:	op de vervaldag van de aanvraag van offertes : de inschrijver van de beste offerte
			
//	Technische controle	94/----
//	VP	:	-	elke combinatie van de cijfers 1 tot 4, zijnde :
//	(W58)	1 : chassis, 2 : stuurinrichting, 3 : ophanging of 4 : reminrichting
//	De letter A zal voor het of de cijfer(s) verschijnen wanneer de uitslag niet P is
//	De letter B zal voor het of de cijfer(s) verschijnen wanneer de uitslag P is
//	-	R	:	om aan GOCA de intrekking van een technische controle per vergissing vermeld in een andere expertise mede te delen
//	-	N	:	geen technische controle
//	VD	:	blanco (technische controle niet vermeld)
//
//	Na het in aanvraag plaatsen voor wrakoffertes is het niet meer toegelaten om 'technische controle' en 'geen technische controle' te wijzigen en vice-versa (WCT).
//	De afsluiting van een expertise en het in aanvraag plaatsen voor wrakoffertes met technische controle die niet blanco en niet N is, zijn geweigerd (WBP) indien het type totaal verlies R, W, X, Y of Z is.
//	De afsluiting van een expertise met technische controle wordt geweigerd (W70) indien het chassisnummer, de datum van het schadegeval of de nummerplaat ontbreekt (of indien de plaat N is).
//	Technische controle niet blanco, niet N en niet R gedwongen tot N (WB7) indien men afsluit met Afstand.
//	De afsluiting van een expertise met technische controle R wordt geweigerd (WB6) indien de uitslag niet A is.
//	Een expertise afgesloten met technische controle wordt gemeld aan GOCA.
//
//	Sleping	96/-
"96"			=>  $objJob->Damage ? $objJob->Damage->Towed : "N",
//	Kosten van depannage	8A/------,--
"8A"			=> $objJob->Damage	? $objJob->Damage->BreakDownCosts : "0",
//	Afzonderlijke voorlopige herstelling	97/-
"97"			=> $objJob->Damage ? $objJob->Damage->ProvisionalRepair : "N",
//	Kosten van voorlopige herstelling	9A/------,--
"9A"			=> $objJob->Damage ? $objJob->Damage->ProvisionalRepairCosts : "0",
//	Bandenprofiel
//	VL	G5/----
"G5"			=> $VL ? $VL->Depth : "0",
//	VR	G6/----
"G6"			=> $VR ? $VR->Depth : "0",
//	AL	G7/----
"G7"			=> $AL ? $AL->Depth : "0",
//	AR	G8/----
"G8"			=> $AR ? $AR->Depth : "0",
//	Reserve	G9/----
"G9"			=> $Reserve ? $Reserve->Depth : "0",
//	Afsluiting of Opgangbrenging	98/-
//	VP	:	R	om de opdracht te herlanceren 
//	(wijzigt de datum van de laatste ingebrachte gegevens)
//	C	om de opdracht af te sluiten
//	Verboden waarde indien de opdracht reeds afgesloten werd of indien een aanvraag van offerte in werking is
//
//	Aanmerkingen expertise
//	Lijn 1	S5/----(75)-------
//	Lijn 2	T0/----(75)-------
//	Lijn 3	T5/----(75)-------
//	Lijn 4	U0/----(75)-------
//	Lijn 5	U5/----(75)-------
//	Lijn 6	V0/----(75)-------
//	Lijn 7	V5/----(75)-------
//	VP	:	willekeurig

//	Aanvraag van offertes
//	Aanmerkingscode van de deskundige	7C/---(14)---
//	VP	:	van 1 tot 7 codes van 2 cijfers, links aangeschoven en begrepen tussen 01 en 17:
//	01	GEWIJZIGD CHASSISNUMMER
//	02	VOERTUIG NOOIT INGESCHREVEN (MET 705)
//	03	LPG VERWIJDERD
//	04	SPECIALE VELGEN VERWIJDERD
//	05	HARDTOP VERWIJDERD
//	06	SPOILER VERWIJDERD
//	07	AIR CONDITIONING VERWIJDERD
//	08	RADIO VERWIJDERD
//	09	CARROSSERIE VERWIJDERD
//	10	LAADBRUG VERWIJDERD
//	11	HIJSKRAAN VERWIJDERD
//	12	KOELGROEP VERWIJDERD
//	13	GSM VERWIJDERD
//	14	TREKHAAK VERWIJDERD
//	15	GEEN STAANGELD
//	16	GEEN AFHAALKOSTEN
//	17	GPS VERWIJDERD
//	Het - teken links aangeschoven laat toe de aanmerking van de deskundige te wissen.
//	De code 15 wordt automatisch doorgevoerd tijdens het invoeren van de aanvraag van offertes indien de hersteller erkend is.
//	De rubriek is enkel toegankelijk voor een bemiddelaar deskundige (WE8) en kan niet meer gewijzigd worden na de aanvraag van offertes (WO6).
//
//	Aanvraag van offertes	J0/------
//	VP	:	-	Y : aanvraag van offertes met 7 dagen verstrijkingstermijn.
//	-	elke mogelijke verstrijkingsdatum (DDMMJJ) van meer dan 7 dagen (*) en minder dan 50 dagen (*) te beginnen bij de aanvraag van offertes (W84).
//	-	elk getal n van 1 of 2 cijfers : aanvraag van offertes met verstrijking binnen n dagen, waarbij n begrepen tussen 7 en 50 dagen (*).
//	(*) Elke termijn lager dan 7 dagen wordt automatisch gedwongen naar 7 dagen (W84).
//	Elke termijn hoger dan 50 dagen wordt automatisch gedwongen naar 50 dagen (W84).
//	-	het - teken : om de aanvraag van offertes te schrappen.
//	Indien de vervaldag een zaterdag of zondag is, wordt deze automatisch naar de vrijdag voordien gebracht (op de volgende maandag als de termijn van de vervaldag minder is dan 9 dagen).
//	Indien de verstrijkingsdatum op een feestdag valt (Nieuwjaarsdag (1/1), feest van de Arbeid (1/5), Nationale feestdag (21/7), Onze Lieve vrouw Hemelvaart (15/8), Allerheiligen (1/11), Wapenstilstand 1918 (11/11), Kerstmis (25/12), Paasmaandag, Hemelvaart of Pinkster maandag), dan wordt deze automatisch gedwongen naar de volgende dag (of, wanneer deze laatste een zaterdag of een zondag is, op de volgende maandag).
//
//	VD	:	blanco
//
//	De rubriek is slechts toegankelijk voor een deskundige-bemiddelaar (WE8), indien er geen aanvraag van offertes reeds gevraagd werd (WO6) en indien de volgende rubrieken ingevuld zijn :
//	-	type AUDATEX	(WA3)	(als het een expertise is)
//	-	merk	(WO8)	(als het een bestek is of een VNC berekening)
//	-	model	(WO1)	(als het een bestek is of een VNC berekening)
//	-	plaat	(WO2)	
//	-	châssisnummer	(behalve indien het type *04 of *05 is) (W8Z)
//	-	kilometers	(WO3)
//	-	datum van eerste in omloopbrenging (WO4)
//	-	overeenstemmend adres met bezoekplaats van het voertuig : schadelijder (W87), hersteller (W88) of elders (W89)
//	-	fotoalbum "aanvraag van offertes " (of algemeen album)
//	-	technische controle of type totaal verlies (WOK)
//	-	technische controle en technisch totaal verlies mogen niet gelijktijdig aanwezig zijn
//
//	De naam van de schadelijder en van het bezoekadres van het voertuig (en het merk in geval van een bestek ), mogen niet gewijzigd worden vóór de verstrijking (of de schrapping) van de aanvraag van offertes
//	Indien de plaat gewijzigd is (of het chassisnummer of de kilomerterstand of datum 1ste in omloopbrenging of een model in geval van een bestek ), dan zal de opdracht opnieuw in het journaal bevinden (met als melding 'wijziging')
//
//	Bezoekplaats van het voertuig in aanvraag van offertes	J1/-
//	VP	:	S	indien het voertuig zich bij de schadelijder bevindt (W87 indien het adres van de schadelijder niet gekend is)
//	R	indien het voertuig zich bij de hersteller bevindt (W88 indien het adres van de hersteller niet gekend is)
//	D	indien het voertuig zich in het depot bevindt (W8M indien het adres van het depot niet gekend is)
//	A	elders (W89 indien bezoekplaats niet gekend is)
//	VD	:	R
//	De rubriek is slechts toegankelijk voor een deskundige-bemiddelaar (WE8) en mag niet meer gewijzigd worden na aanvraag van offertes (W06)
//
//	Adres van bezoekplaats :
//	-	naam	J2/---(30)---
//	VP	:	willekeurig
//
//	-	straat	J4/---(30)---
//	VP	:	willekeurig
//
//	-	postcode	J6/----
//	VP	:	numeriek (W)
//
//	-	gemeente	J7/---(25)---
//	VP	:	willekeurig
//
//	Deze rubrieken zijn slechts toegankelijk voor een deskundige-bemiddelaar (WE8) en mogen niet meer gewijzigd worden na de aanvraag van offertes (WO6)
//
//	Verzending van een brief aan de hersteller	J9/-
//	VP	:	Y	een brief voor aanvraag van offertes wordt ofwel direct via de postbus van de hersteller verstuurd, ofwel via de postbus van de deskundige (W88 indien het adres van de hersteller niet gekend is)
//	N	geen brief verstuurd (W53)
//	VD	:	de optie 'zending van een brief aan de hersteller' van de voorschriften van de deskundige 
//	gedwongen tot N (WO9) bij de aanvraag van offertes indien het adres van de hersteller niet gekend is
//	De rubriek is slechts toegankelijk voor de deskundige-bemiddelaar (WE8) en mag niet meer gewijzigd worden na de aanvraag van offertes (WO6)
//
//	Nummer van de opdracht waarvan men de aanvraag van offertes wil overnemen	4A/--(7)--
//	De rubriek is slechts toegankelijk voor de deskundige-bemiddelaar (WE8), indien geen enkele aanvraag van offertes reeds werd gevraagd (WO6)
//
//	VP	:	-	nummer van de opdracht waarvan de deskundige de aanvraag van de offertes wil overnemen 
//	De deskundige moet de toelating geven voor het overnemen van de aanvraag van de offertes (WOB) en moet worden vermeld in deze opdracht (WOE) 
//	Deze opdracht moet bestaan (WOC), een aanvraag van offertes (niet geschrapt) en die niet werd overgenomen (WOH) moet gelanceerd zijn (WOG) 
//	De deskundige die er tussenkomt moet de overname van de aanvraag van offertes toelaten (WOF)
//	-	0000000 in een expertise : indien men het nummer van de opdracht niet kent waarvan men de aanvraag van offertes wil overnemen 
//	Het chassisnummer (8Z) en de volledige datum van het schadegeval (WCB) zijn verplicht
//	Indien de opdracht waarvan men de aanvraag van offertes wil overnemen en indien de expertise de voorwaarden eerbiedigt (cfr. Gevraagde aanvraag van offertes), dan zal zij in aanvraag van offertes geplaatst worden: een datum of een verstrijkingsdatum kan dan ingevoerd worden (bij gebrek: 7 dagen)

//	Audatex
//
//	Type en model	L9/--(6)-
//	VP	:	elk model en type AUDATEX dat vermeld is op een expertise formulier : 
//	1 letter of 1 cijfer, gevolgd door 4 of 6 cijfers (RXC) (000000 = denkbeeldig model) 
//	(99999 of 999999 = het model en type AUDATEX evenals de standaard opties worden automatisch afgeleid van het VIN chassisnummer (RXC indien deze laatste ontbreekt of onjuist is).
//
//	Uurloon	
//	-	*	L5/----
//	-	**	L6/----
//	-	***	L7/----
//	-	L	L8/----
//	VP	:	elke numerieke positieve waarde of niets (R61)
//
//	Uitvoeringen	
//	-	Codes (maximum 69 codes)	K0/----(138)----
//	VP	:	cfr. gebruiksaanwijzing van de expertise formulieren AUDATEX
//
//	-	Toegevoegde beschrijvingen
//	-	eerste	O2/----(20)-----
//	-	tweede	O4/----(20)-----
//	-	derde	O6/----(20)-----
//	VP	:	willekeurig
//
//	-	Gewijzigde beschrijvingen
//	-	eerste	-	code	M7/--
//	-	omschrijving	M8/----(19)----
//	-	tweede	-	code	P7/--
//	-	omschrijving	P8/----(19)----
//
//	code :
//	VP	:	cfr. gebruiksaanwijzing van de expertise formulieren AUDATEX
//	omschrijving :
//	VP	:	willekeurig (een * wordt automatisch op de 20ste positie bijgedrukt)
//
//	Globale Codes (maximum 20 globales codes)	M5/---(12)----
//	-	Code (pos. 1 en 2)	XX	
//	VP	:	cfr. gebruiksaanwijzing van de expertise formulieren AUDATEX (WA9)
//
//	-	Waarde (pos. 3 tot 12)	X(10)	
//	VP	:	elke numerieke waarde (R)
//
//	Standaardbewerkingen (maximum 512 standaardbewerkingen en wijzigingen)	N1/---(15)------
//	-	bewerking code (pos. 1 tot 3)	X(3)	
//	VP	:	(links aangeschoven) cfr. gebruiksaanwijzing van de expertise formulieren AUDATEX (RA7)
//
//	-	index (pos. 4 tot 7)	X(4)	
//	VP	:	3 cijfers links aangeschoven of 4 cijfers cfr. gebruiksaanwijzing van de experteseformulieren AUDATEX
//
//	-	aantal AE (pos. 8 tot 11)	X(4)	
//	VP	:	-	ofwel het teken '-' (links aangeschoven) (indien repertorium afwijkend is van N (W)) : de standaard bewerking code/index dient verwijderd te worden uit de gegevens waarop men zich steunt
//	-	ofwel elke numerieke gehele positieve waarde of niets (R)
//
//	-	reserve	X	
//
//	-	% sleet (pos. 13 tot 15)	XXX	
//	VP	:	elke gehele numerieke waarde > 0, < 100 (R)
//
//	Wijzigingen (De wijziging(en) duid(t(en) op de voorafgaande bewerking)	N3/--(7)--
//
//	-	van de prijs van het onderdeel in EUR
//	VP	:	elke numerieke positieve waarde (of nul op voorwaarde dat het aantal AE nul is)
//
//	-	van de prijs van het onderdeel in %, bijvoegen of aftrekken	N4/----
//	VP	:	elke numerieke waarde >-100 < 100 (toe te voegen of af te trekken van de prijs)
//	N.B.:	Deze 2 rubrieken zijn onderling exclusief (W), en verboden (W) indien de bewerking niet E of ET is
//
//	-	van de AE naar EUR	N5/--(7)--
//	VP	:	elke gehele numerieke positieve waarde of nul
//	Aantal AE en AE in EUR : deze 2 rubrieken
//	-	zijn onderling exclusief
//	-	1 van de 2 is verplichtend indien de bewerking IE, NE of PE is
//	-	verboden indien de bewerking TE of LV is
//
//	Index en zonenummer van de PAD. Data voorbehouden voor de PAD	P0/---(10)---
//	Niet-standaardbewerkingen (maximum 154 niet-standaardbewerkingen)	
//	-	1e deel	P1/-----(15)------
//	-	bewerking code (pos. 1 tot 3)	XXX	
//	VP	:	(links aangeschoven) cfr. lijst van de toegelaten waarden in de gebruiksaanwijzing van de expertise formulieren (RA7) (en SR : SMART REPAIR, voorbehouden voor de PAD)
//
//	-	index (pos. 4 tot 7)	X(4)
//	VP	:	willekeurig
//
//	-	Prijs (pos. 8 tot 15)	X(8)
//	VP	:	elke numerieke positieve waarde of niets (R)
//
//	-	2de deel	
//	-	omschrijving	P2/-----(19)------
//	VP	:	willekeurig
//
//	-	3de deel	
//	-	referentienummer	P4/-----(15)------
//	VP	:	willekeurig
//
//	-	4de deel	P5/--(9)---
//	-	aantal AE (pos. 1 tot 4)	X(4)	
//	VP	:	-	ofwel het teken '-' (links aangeschoven) 
//	(indien repertorium verschillend is van N (W)) : de niet-standaardbewerkingscode/index is te verwijderen uit de gegevens waar men zich op steunt
//	-	ofwel elke gehele numerieke positieve waarde of nul (R)
//
//	-	reserve	X
//	-	% sleet (pos. 6 tot 8)	X(3)
//	VP	:	elke gehele numerieke waarde > 0 , < 100 (R)
//
//	-	AE klas (pos. 9)	X
//	VP	:	blanco, 1, 2 of 3 (W)
//	Prijs en aantal UT zijn wederzijds exclusief (W) voor de bewerkingen anders dan E, ET en TE
//
//	Denkbeeldig model :
//	-	fabrikant	N6/-----(18)------
//	VP	:	willekeurig, ofwel het teken '-' te verwijderen
//
//	-	model	N8/-----(18)------
//	VP	:	willekeurig
//
//	-	type	O0/-----(18)------
//	VP	:	willekeurig
//
//	-	AE/uur	O8/--
//	VP	:	10 en 12 (WA8)
//	VD	:	10
//
//	Honoraria
//
//	Aard van tarifering	B3/-
//	VP	:	-	I indien er een rooster voor tarifering bestaat (WH1)
//	-	E
//
//	Hoofdbedrag	B4/------,--
//	VP	:	elke positieve numerieke waarde of nul
//	indien niet nul, dan wordt de aard van tarifering E
//
//	Verplaatsingen	I3/------,--
//	VP	:	elke positieve numerieke waarde of nul
//
//	Bijkomende vacaties	I4/------,--
//	VP	:	elke positieve numerieke waarde of nul
//
//	Foto's	I5/------,--
//	VP	:	elke positieve numerieke waarde of nul
//
//	Administratieve kosten	Y8/------,--
//	VP	:	elke positieve numerieke waarde of nul
//
//	Diverse	I6/------,--
//	VP	:	elke positieve numerieke waarde of nul
//
//	Transit honoraria	B6/-
//	VP	:	Y en N (W53)
//	VC	:	keuze van deskundige indien expertise 
//	N indien bestek
//
//	Aanmerking	B7/----(20)-------
//	VP	:	willekeurig
//
//	Zendingnaar mij	X0/---(9)---
//	VP	:	-	blanco	=	niets
//	-	A	=	alles (volgorde 0312456789)
//	-	de tekens
//	0	=	Beknopt verslag
//	1	=	Proces-verbaal
//	2	=	Tegensprekelijk akkoord
//	3	=	Identificatie
//	4	=	Follow-up
//	5	=	Honoraria
//	6	=	Akkoord direkte betaling
//	7	=	AUDATEX-gegevens
//	8	=	Tabel met de ontvangen offertes
//
//	VD	:	blanco
//
//	Opmerking:	De schikking van de tekens bepaalt tevens de volgorde van afdrukken
//	Voorbeeld : 41 geeft aanleiding tot het afdrukken van de follow-up, vervolgens het proces-verbaal
//
//	De zending naar mij in (B)EF of (E)UR ?	8C/-
//	VP	:	B en E
//	VD	:	de munteenheid van de bemiddelaar
//
//	Deponering in de bus van	X1/--(7)--
//	VP	:	-	blanco	=	niets
//	-	de tekens (W75)
//	A	=	Lastgever (*)
//	B	=	Deskundige (*)
//	C	=	Hersteller (*)
//	D	=	Makelaar (*)
//	E	=	Mij tegenpartij (*)
//	F	=	2de deskundige (*)
//	G	=	3de deskundige (*)
//
//	(*)	De bemiddelaar moet : deelnemen aan deze opdracht, bestaan in de identificatie (WN5, WN6, WN4, WN3, WN7, WN8) en de elektronische overseiningen aanvaarden (WC9, WE2, WR2, WP2, WCA, WEA)
//
//	VD	:	blanco, maar
//	1)	A is automatisch
//	-	bij instelling van opdracht door de maatschappij, welke dit opgegeven heeft bij haar voorschriften
//	-	bij instelling van opdracht door de deskundige of de makelaar, indien opgegeven in hun voorschriften (bijzondere of algemene)
//	2)	B is automatisch bij instelling van opdracht door de maatschappij of de makelaar
//	3)	A, B en/of D zijn automatisch bij afsluiting van de opdracht, indien dit zo automatisch opgegeven werd in de voorschriften van de lastgever, de deskundige en/of de makelaar
//	4)	Tijdens het geven van een opdracht, is er een automatische deponering
//	-	doch aanzien als aangevraagd
//	-	in de bus van de bemiddelaar die de opdracht geeft
//	(dit laat toe - indien nodig - om het nummer dat aan de opdracht toegekend werd terug te vinden door een eenvoudige heropvraging van de bus (lijst 09)
//
//	en/of van nummer	X2/---(9)---
//	VP	:	-	blanco : niets
//	-	elk in de bestanden bestaand bemiddelaarsnummer (WN5, WN6, WN4, WN3) en welk de elektronische overseiningen aanvaardt (WC9, WE2, WR2, WP2), inbegrepen een bemiddelaar welke aan de opdracht deel neemt (opgepast in betrekking met inhoud van zending en fakturatie)
//	VD	:	blanco
//
//	Nota: Aanmerkingen omtrent deponeringen
//
//	1.	Verzendingen in funktie van geadresseerde
//	1.1.	expertise
//
//	niet afgesloten	afgesloten							
//	A	Lastgever	3, 4	0, 1, 3, 4, 5, 6							
//	B	Deskundige	1, 3, 4, 5	0, 1, 3, 4, 5, 6							
//	C	Hersteller	3, 4	0, 1, 3, 4, 6							
//	D	Makelaar	3, 4	0,1,3							
//	E	Maatschappij tegenpartij	3, 4	0,1, 3, 4							
//	F	2de Deskundige	3, 4	0, 1, 3, 4							
//	G	3de Deskundige	3, 4	0, 1, 3, 4							
//	Nr van derde	1, 3, 4	0, 1, 3, 4							
//
//
//	1.2.	bestek (afgesloten of niet)
//
//	A	Lastgever	1, 3, 4	
//	B	Deskundige	0 (**), 1, 3, 4, 5, (*) 7	
//	C	Hersteller	1, 3, 4	
//	D	Makelaar	1, 3, 4	
//	E	Maatschappij tegenpartij	1, 3, 4	
//	F	2de Deskundige	1, 3, 4	
//	G	3de Deskundige	1, 3, 4	
//		Nr van derde	1, 3, 4	
//
//
//	*	Indien de verzender een hersteller is
//	**	Wanneer het bestek is afgesloten.
//
//	Volgorde van deponeringen : 0, 3, 1, 2, 4, 5, 6, 7, 8
//	Bij de verstrijking van een aanvraag van offertes, worden de delen 3, 4 en 8 (tabel van de ontvangen offertes) in de postbus van de deskundige en de maatschappij gedeponeerd indien deze bemiddelaars het wensen op gebied van hun voorschriften.
//	Soms wordt een deel niet gedrukt.
//	Voorbeeld	:	deel 5 wordt niet gedrukt als de honoraria niet werden ingevoerd (cfr INFLI09)
//
//	N.B.:	De beperkingen van afgifte waarvan sprake in 1.1 en 1.2 worden noch toegepast in geval van ontvangst in recordvorm door de bestemmeling, noch in geval van uitdrukkelijke verzendingen van rubriek x1 en x2.
//
//	2.	De tellers
//	2.1.	expertise
//	De deponeringen in de bus van A (Lastgever)	start de teller M/
//	De deponeringen in de bus van D (Makelaar)	start de teller T/
//	De deponeringen in de bus van B (Deskundige)	start de teller D/
//	De deponeringen in de bus van C (Hersteller)	start de teller H/
//	De 'zendingen naar mij', de deponeringen in de postbus van E, F en G en een 'derde' starten de tellers M/, T/ en D/ op naargelang de afzender, de maatschappij, de tussenpersoon of de deskundige is.
//	Opgepast: (cfr INFLI12en INFLI13) : 
//	De maatschappij betaalt de bijkomende verzendingen van M/ en T/ 
//	De deskundige betaalt de bijkomende verzendingen van D/ en H/
//
//	2.2	bestek
//	De bemiddelaar bestek opsteller betaalt alle bijkomende verzendingen.
//	Gewoon expert	1B/-----
//	Een expertise -opdracht maakt deel uit van een expertise circuit indien, bij de aanstelling door de maatschappij, naast de 'normale' deskundige, een gewone deskundige (bestaand en aangesloten) vermeld wordt.
//	Een bestaand hersteller, aangesloten en erkend moet eveneens vermeld worden. Indien dit niet in het geval is, wordt de opdracht buiten het expertise circuit beschouwd en de aangestelde deskundige is de gewone deskundige.
//
//	Controle-expert	2B/-----
//	Rubriek voorbehouden aan de maatschappij bij de aanstelling
//	VP	:	elke bestaande en aangesloten deskundige
//	Deze laatste controleert:
//	-	buiten expertise circuit: de normale deskundige
//	-	expertise circuit :	-	de gewone deskundige indien hij aangesteld werd
//	-	anders, de hersteller
//	Nota:
//	Er is geen controle indien het nummer van de controle deskundige niet vermeld wordt of gelijk is aan het nummer van de gewone deskundige (expertise circuit) of gelijk is aan het nummer van de normale deskundige (buiten expertise circuit)
//
//	bestek voor expertise nummer	5A/--(7)--
//	VP	:	elk bestaand expertise nummer van het nieuwe circuit dat nog niet afgesloten is en bij de onderzoekscel
//	Rubriek van een bestek , voorbehouden aan de hersteller vermeld in de expertise
//
//	Expert vereist	0B/-
//	VP	:	Y	=	de hersteller wenst het 'onmiddellijk' bezoek van een deskundige
//	N	=	annuleert deze aanvraag
//	Rubriek van een bestek , voorbehouden aan de hersteller
//
//	Conclusie van de hersteller	9I/-
//	1	=	de berekening is conform met de voorziene herstelling
//	2	=	het voertuig is waarschijnlijk een totaal verlies
//	3	=	het voertuig heeft meerdere schadezones van verschillende oorsprong
//	4	=	de berekening stemt overeen maar een technisch advies van de deskundige is noodzakelijk
//	5	=	de berekening stemt overeen maar het voertuig vereist een technische controle
//	6	=	door de omvang van de schade is een deskundige ter plaatse vereist
//	Rubriek van een bestek , voorbehouden aan de hersteller
//	Betaling van de voertuigen
//	De deskundige, die beslist over te gaan tot de verkoop, geeft de aanvraag tot betaling in via een scherm van zijn programma en verzendt deze naar Informex. Een "Aanvraag tot betaling"wordt gedeponeerd in de electronische postbus van de koper, met een copie aan de deskundige, aan de maatschappij en aan de hersteller (indien aangesloten).
//	De koper betaalt, via een overschrijving met gestructureerde mededeling, op een bankrekening die hiervoor bestemd is. Wanneer deze betaling ontvangen en gevalideerd wordt door Informex, wordt een "Afhaalbon "in de electronische postbus van de koper gedeponeerd, met copie aan de deskundige, aan de maatschappij en aan de hersteller (indien aangesloten).
//	Indien de koper op dezelfde bankrekening een bedrag op zijn voertuigrekening zou storten, kan hij via een scherm van zijn programma, de betaling uitvoeren via het debet van zijn rekening. In dit geval wordt de "Afhaalbon" onmiddellijk gedeponeerd. (Via ditzelfde scherm, kan de koper vragen om de volledige of een gedeelte van het bedrag op zijn wrakkenrekening te recupereren).
//	Wanneer geen enkele betaling uitgevoerd werd na de verstrijking van de betalingstermijn, worden de deskundige en de maatschappij hiervan verwittigd (de uiterste datum is de datum van aanvraag vermeerderd met 12 dagen of de eerste werkdag die volgt indien deze op een zaterdag, een zondag of een feestdag valt).
//	De technische afsluiting wordt geweigerd zolang de aanvraag niet geannuleerd of betaald is.
//	Bij de maandelijkse boekhoudkundige afsluiting wordt een lijst met de ontvangen betalingen gedeponeerd in de electronische postbus van de maatschappij, van de deskundige en van de koper, namelijk het "Overzicht van de betalingen van de voertuigen".
//	Aanvraag tot betaling
//	Wanneer in een niet afgesloten expertise , een aanvraag van offertes verstreken is (en waarin dus het nummer van de koper en het bedrag van zijn bieding vermeld staat), kan de deskundige (en enkel hij) de hieronder vermelde data inbrengen.
//
//	De aanvraag tot betaling is geldig wanneer het te betalen bedrag door de koper (waarde van de offerte + BTW van het voertuig - depannagekosten - staangeld) hoger is dan 0 (ROM).
//	Zolang geen enkele betaling gevalideerd werd, kan de deskundige de hieronder vermelde data wijzigen, evenals het bedrag van de bieding en het nummer van de koper (in dit laatste geval, wordt de datum van de aanvraag tot betaling gewijzigd en zodus ook de uiterste betaaldatum), volgens de regels van de maatschappij.
//	Na validering van een betaling, kan de deskundige het bedrag van de bieding nog verminderen of de aanvraag tot betaling annuleren. De voertuigrekening wordt dan gecrediteerd.
//	De betaling door Informex moet uitgevoerd worden	0E/-
//	VP	:	1 : aan de maatschappij	
//	2 : aan de deskundige (behalve indien aanstelling door de maatschappij) (ROL)	
//	VD	:	1	
//	Depannagekosten	1E/------,--
//	VP	:	elk bedrag incl. BTW*, positief of nul	
//	VD	:	0	
//	* In het ingave scherm duidelijk een bedrag incl. BTW ingeven: in de andere schermen worden de bedragen excl. BTW aangeduid.
//	Staangeld	2E/------,--
//	VP	:	elk bedrag incl. BTW*, positief of nul	
//	VD	:	0	
//	* In het ingave scherm duidelijk een bedrag incl. BTW ingeven: in de andere schermen worden de bedragen excl. BTW aangeduid.
//	Of	
//	Staangeld per dag	3E/------,--
//	VP	:	elk bedrag incl. BTW*, positief of nul	
//	VD	:	0	
//	* In het ingave scherm duidelijk een bedrag incl. BTW ingeven: in de andere schermen worden de bedragen excl. BTW aangeduid.
//	Het staangeld wordt gedwongen naar 0, indien de hersteller die vermeld wordt in de expertise , door de maatschappij erkend is. 
//	Deze zijn wederzijds exclusief.
//	En	
//	Aantal dagen staangeld	4E/---
//	VP	:	elke numerieke waarde, positief of nul	
//	VD	:	0	
//	Einddatum staangeld voor de maatschappij	5E/DDMMJJ
//	VP	:	elke samenhangende datum DDMMJJ later of gelijk aan de uiterste datum van betaling	
//	Indien de ingegeven datum voor de uiterste betaaldatum valt, dan wordt de datum vervangen door de laatst genoemde.
//	De ingave van deze rubriek leidt de aanvraag tot betaling in.	
//	De factuur van de kosten betaald door de koper aan de hersteller wordt	6E/-
//	VP	:	1 : door de koper ter beschikking gehouden van de maatschappij en de deskundige,	
//	2 : door de koper verzonden naar de deskundige	
//	VD	:	2	
//	Deze data wordt genegeerd indien er geen kosten zijn.	
//	De documenten zullen	7E/-
//	VP	:	1 : in het voertuig zijn	
//	2 : verzonden worden door de deskundige na betaling	
//	3 : verzonden worden door de deskundige na ontvangst van de factuur van de kosten betaald aan de hersteller	
//	VD	:	2	
//	3 wordt gedwongen naar 2 indien er geen kosten zijn of indien de factuur ervan bij de koper moet blijven.	
//	Inschrijvingsbewijs	8E/-
//	VP	:	Y : bestaat	
//	N : bestaat niet	
//	Z : bestaat niet, een verliesattest bestaat	
//	Certificaat van overeenstemming	9E/-
//	VP	:	Y : bestaat	
//	N : bestaat niet	
//	Z : bestaat niet, een verliesattest bestaat	
//	Bewijs van technische controle	0F/-
//	VP	:	Y : bestaat	
//	N : bestaat niet	
//	X : niet van toepassing	
//	Onderhoudsboekje	1F/-
//	VP	:	Y, N en X	
//	Radiocode	2F/-
//	VP	:	Y, N en X	
//	Valsheidsattest van het chassisnummer	3F/-
//	VP	:	Y, N en X	
//	Installatieattest LPG	4F/-
//	VP	:	Y, N en X	
//	Sleutel(s)	5F/-
//	VP	:	van 0 tot 9 en X (niet van toepassing)	
//	Afstandsbediening(en)	6F/-
//	VP	:	van 0 tot 9 en X (niet van toepassing)	
//	Alarmsleutel(s)	7F/-
//	VP	:	van 0 tot 9 en X (niet van toepassing)	
//	Annulatie van de aanvraag tot betaling	8F/-
//	VP	:	Y	
//	BTW van het voertuig	9F/------,--
//	VP	:	elk positief bedrag of nul	
//	VD	:	0	
//	Deze data is verboden indien er geen aanvraag tot betaling lopende is of indien één van de data hierboven vermeld gelijktijdig ingebracht worden.
//	Bij de ingave van deze rubriek , in het gedeelte "Aanvraag tot betaling van het voertuig" wordt de opmerking gewijzigd door: "AANVRAAG TOT BETALING GEANNULEERD ", met een copie in de postbus van de koper, de deskundige, de maatschappij en die van de hersteller (indien aangesloten).
//
//
//
//	Het gedeelte van de PV "Aanvraag tot betaling van het wrak ", dat kan afgedrukt worden, bevat onder andere:
//	- de opmerking DIT IS GEEN AFHAALBON,
//	- de einddatum voor staangeld, 
//	- de som door de koper te betalen (bieding min de betaalde kosten incl. BTW),
//	- de te gebruiken referentie (mededeling).
//	Met deze 00 kan men deze bepaalde referentie (mededeling) onderscheiden van de referentie die door de koper moet gebruikt worden om zijn voertuigrekening aan te vullen: zijn ondernemingsnummer (10 cijfers), waarvan de twee laatste cijfers de aanvulling zijn van 97 van de modulo 97 (welke nooit gelijk zijn aan 00) van de 8 eerste.
//	Om het formalisme van de overschrijvingen met gestructureerde mededeling te respecteren (3 + 4 + 5 cijfers), zullen deze referenties gevolgd worden door hun modulo 97 (indien deze laatste nul is, wordt hij gedwongen naar 97).
//
//	SCHEMA BESTAANSBEELD VAN EEN OPDRACHT
//	1)	Oprichting :
//	-	Door de Maatschappij.
//	-	Door de deskundige of makelaar indien toestemming van maatschappij.
//	-	Door iedereen indien bestek .
//
//	2)	Wijziging :
//	-	Mogelijk door de Mij tot wanneer de expertise is begonnen (datum laatste wijziging van expertise = 0).
//	-	Mogelijk door de deskundige tot wanneer de expertise afgesloten wordt
//	-	Door de belanghebbende, indien bestek , tot wanneer deze afgesloten wordt
//
//	3)	Technische afsluiting :
//	-	Deze gebeurt vrijwillig door de deskundige of de bestek opsteller.
//
//	4)	Boekhoudkundige afsluiting :
//	-	Elke tijdens de maand technisch afgesloten opdracht wordt opgenomen in de boekhoudkundige afsluiting van de betreffende maand.
//
//	Kredietopening (direkte betaling)
//	Voorwaarden :
//	1.	de maatschappij gebruikt de direkte betaling
//	2.	de tussenkomst is hoger dan nul
//	3.	hersteller : aangeduid, aangesloten, erkend indien de maatschappij erkent
//	4.	gewenste uitslag : 1 of F
//	5.	indien een AUDATEX-berekening gemaakt werd : deze dient gekopieerd te worden van het bestek van de hersteller via het repertorium
//	6.	grensbedrag niet expliciet nul
//
//	Berekening van de tussenkomst :
//	Kostprijs van de herstelling	:	waarde eerste schatting of forfait waarde + bedrag BTW
//	-	niet te betalen bedrag
//	-	vrijstelling (indien af te trekken)
//	-	terugvorderbaar gedeelte van de BTW
//
//	Wanneer :
//	-	indien de voorwaarden vervuld zijn
//	-	bij de technische afsluiting indien het grensbedrag hoger of gelijk is aan de tussenkomst
//	-	na de technische afsluiting op het ogenblik van de ingave van een grensbedrag hoger of gelijk aan de tussenkomst
//	Het is onmogelijk een krediet te schrappen na de opening
//
//	Exemplaren :
//	-	de maatschappij : het PV van de sluiting of het PV gevraagd tijdens de ingave van het grensbedrag
//	-	de hersteller : automatische zending van een PV met daarin vervat het Akkoord tot direkte betaling
//
//	Opmerkingen:
//	1.	VC	:	waarde door afwezigheid bij het instellen van de opdracht
//	R'	:	verwerping indien de opdracht een expertise is
//	waarschuwing indien de opdracht een bestek is
//	R "	:	verwerping van de AUDATEX-berekening
//
//	2.	Rubrieken welke door een bijwerking de wijziging meebrengen van de datum van laatste bijwerking aanstelling :
//	, 12, 13, 14, 15, 16, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 35, 37, 39, 40, 42, 44, 46, 47, 49, 50, 51, 52, 53, 56, 59, 60, 63, 64, 65, 67, 68, 74, 75, 77, 78, 80, 81, 87, E3, E4, E5, E6, F3, F4, F5, , F7, F8, G0, G1, G2, G3, I9, O9, Q5, R0, R5, S0, W0, W1, W2, , W4, W5, X6, Y1, Y2, Y3, Y5, Y6, Z0, Z2, Z3, Z4, Z5, 0A, 1A, 2A, 3A, 7B, 8B, 9B, 0C, 1C, 9C, 1D, 2D, 3D, 4D, 5D.
//
//	3.	Rubrieken welke door een bijwerking de wijziging meebrengen van de datum van laatste bijwerking expertise :
//	73, 85, 86, 88, 90, 91, 92, 94, 95, 96, 97, E1, E9, F0, F9, G5, G6, G7, G8, G9, H0, H1, H2, H3, H4, H5, H6, H8, I0, I1, J0, J1, J2, J4, J6, J7, J9, S5, T0, T5, U0, U5, V0, V5, Y4, Y7, 4A, 8A, 9A, 0B, 4B, 5B, 6B, 7C, , 6D, 9D, 0E, 1E, 2E, 3E, 4E, 5E, 6E, 7E, 8E, 9E, 0F, 1F, 2F, 3F, 4F, 5F, 6F, 7F, 8F.
//	de AUDATEX-rubrieken.
//
//	4.	Genegeerde rubrieken indien de opdracht een bestek is :
//	12, 27, 28, 59, 73, 87, B6, O9
//
//	5.	Verboden rubrieken bij wijziging expertise (W50) :
//	12, 14 (uitgezonderd voor een bezoldigd expert), 19
//
//	6.	Rubrieken van een expertise voorbehouden aan de deskundige (WE8) :
//	73, 85, 86, 88, 90, 91, 92, 94, 95, 96, 97, 98, B3, B4, B6, B7, B8, E1, E9, F0, F9, G5, G6, G7, G8, G9, H0, H1, H2, H3, H4, H5, H6, H8, I0, I1, I3, I4, I5, I6, J0, J1, J2, J4, J6, J7, J9, S5, T0, T5, U0, U5, V0, V5, Y4, Y7, Y8, 4A, 7C, 6D, 0E, 1E, 2E, 3E, 4E, 5E, 6E, 7E, 8E, 9E, 0F, 1F, 2F, 3F, 4F, 5F, 6F, 7F, 8F.
//
//	7.	Verboden rubrieken (WA3)
//	-	indien de opdracht een expertise is en de bemiddelaar geen deskundige
//	-	of indien het nummer van expertise formulier AUDATEX blanco is : de AUDATEX- rubrieken
//
//	8.	Rubrieken van een expertise wijzigbaar door de Mij of de makelaar
//	-	na het begin van de expertise : 23, 51, E3, 5D
//	-	na het technisch afsluiten : 27, 28, 59, 87, O9
//	Aanvraag tot betaling van de hersteller
//
//	SLEUTEL
//	*	Opdracht	-------
//	1     7
//
//	De SLEUTEL dient het nummer van een expertise opdracht te zijn
//	-	bestaand (R)
//	-	technisch afgesloten (R)
//	-	waarvan de vermelde hersteller de bemiddelaar is die de aanvraag doet (R)
//	-	waarvoor een krediet geopend werd (R) dat niet vervallen is (R) (vervaldatum van het krediet : laatste dag van de 2de maand die volgt op de kredietopening)
//	-	waarvoor de aanvraag tot betaling nog niet werd gedaan (R)
//	en mag slechts volgende 5 DATA bevatten :
//
//	DATA
//	*	Nr van de faktuur	70/--(7)--
//	VP	:	willekeurig, verplicht niet blanco (R)
//
//	*	Gevraagd bedrag	71/------,--
//	VP	:	elke positieve numerieke waarde lager of gelijk aan de tussenkomst (R)
//
//	Gevraagd bedrag voor de depannage	6A/------,--
//	VP	:	elke positieve numerieke waarde lager of gelijk aan het bedrag bepaald door de expert
//
//	Gevraagd bedrag voor de voorlopige herstelling	7A/------,--
//	VP	:	elke positieve numerieke waarde lager of gelijk aan het bedrag bepaald door de expert
//
//	Vervangingsvoertuig ?	72/-
//	VP	:	Y, N (R)
//	VD	:	N
//
//	Deze vraag zet de automatische zending in
//	- naar de maatschappij van een PV,
//	- naar de hersteller van de 'uitvoering van direkte betaling'.

	
);