<?php

namespace Informex;

class Build extends System
{
    private $post;
    private $properties;
    private $env    =   'prod';
    public $string;
	public $strInformex;

    public function __construct($data = false, $strInformex=null) 
    {
		$this->strInformex = $strInformex;
        $this->properties   =   include_once ('lib/update_fields_properties.php');

        if($data && !empty($data) && is_array($data) && $this->env  ==  'prod')
        {
            $this->post =   $data;
            
            $this->build();
        }
        elseif($this->env   ==  'dev')
        {
            $this->post = $this->test_data_array();
            
            $this->build();
        }
        else
        {
            return false;
        }
    }
    
    private function build()
    {
        $new_array  =   array();

        //to do: check type of the string values
        foreach ($this->post as $key    =>  $value)
        {
            if(array_key_exists($key, $this->properties['length']))
            {
                if(!empty($value)   &&  strlen($value)   <=  $this->properties['length'][$key])
                {
                    $new_array[]    =   $key;
                    $new_array[]    =   $value;
                }
            }
            else
            {
                if(!empty($value))
                {
                    $new_array[]    =   $key;
                    $new_array[]    =   $value;
                }
            }
        }
        $this->string   =   '@A@504@K@'.$this->strInformex.'@'.implode('@',$new_array).'@X0@A@';
    }
    
    public function get_update_string()
    {
        return $this->string;
    }
    
    private function test_data_array()
    {
        return  array(
            '24' => '120318',
            '9B' => '5',
            'Z5' => 'POLICYNUMBER 598745554',
            '23' => '9876543210',
            'G1' => '1X9832',
            'Y1' => '500.00',
            'Y2' => '5987.00',
            '26' => '500.00',
            '4D' => 'Y',
            '7G' => 'Y',
            '12' => 'I',
            '14' => '',
            '16' => 'KENNY VAN DE VELDE',
            '15' => '12346',
            '6I' => '6789457',
            '13' => '',
            'E4' => '123456',
            '3D' => '060418',
            '19' => '',
            '20' => '',
            '22' => '123654789',
            '25' => '',
            'G2' => 'AMOUNT',
            '59' => 'Y',
            '27' => 'I',
            '28' => '',
            'O9' => '875.00',
            '87' => '',
            '0G' => '1',
            '9C' => 'BE 0123456546',
            '1D' => '#REFLEAS',
            '32' => 'BE123',
            'E5' => '#REFBROK',
            '35' => 'KBC',
            '37' => 'FRANKRIJKLEI',
            '39' => '2000',
            '40' => 'ANTWERPEN',
            'W5' => '',
            '42' => 'PAMU BVBA',
            '44' => 'RUBENSHEIDE',
            '46' => 'KAPELLEN',
            '50' => '',
            '2G' => '',
            '52' => '',
            '49' => 'N',
            '51' => '',
            'E3' => '',
            '53' => 'BE 0444.498.936',
            '78' => 'CARROSSERIE RIETJE',
            '56' => 'ANTWERPSESTEENWEG',
            '80' => '2950',
            '81' => 'KAPELLEN',
            'Y3' => '',
            '2I' => 'BE 0444.498.936',
            'G3' => 'TOYOTA',
            '75' => 'TUNDRA',
            'G0' => '1HTZ069',
            '60' => '00000000000000001',
            'Y5' => '25000',
            '77' => '',
            '63' => '5700',
            '64' => '1600',
            'Y6' => '100',
            '65' => '7',
            '67' => '010416',
            'I9' => '010418',
            '0A' => 'N',
            '7B' => 'Y',
            '68' => 'Y',
            '1A' => '010418',
            '2A' => 'E',
            '3I' => '1',
            '4I' => '15',
            '3A' => 'Y',
            '95' => '24.00',
            '86' => 'P',
            '92' => 'I',
            '73' => '0',
            '4B' => 'N',
            'F9' => '',
            '88' => '',
            'H1' => '',
            'H2' => '0',
            'H3' => '0',
            'F0' => '0',
            '6D' => '0',
            '5B' => '',
            '96' => 'Y',
            '8A' => '150.00',
            '97' => 'Y',
            '9A' => '500.00',
            'G5' => '0',
            'G6' => '0',
            'G7' => '0',
            'G8' => '0',
            'G9' => '0',
        );
    }
}
