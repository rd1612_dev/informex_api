<?php
namespace Informex;
error_reporting('E_ERROR');
class Parser extends System
{
	
    public $blocks  =   array();
    
    private $data;
    private $system;
    private $Bloknummer_09;
    private $form_type;
    private $edition;

    const ENTITY_TYPES  =   'OPDRACHTGEVER|HERSTELLER|DESKUNDIGE|MAKELAAR';

    public function __construct()
    {
        $this->system   =   new System();
            
        $this->Bloknummer_09           =   include_once (__DOCROOT__.'/vendor/insypro/informex/lib/infli09.php');
        $this->Bloknummer_44           =   include_once (__DOCROOT__.'/vendor/insypro/informex/lib/infli44.php');
        
        if(is_dir(System::DATA_OUT_PATH))
        {
            if($this->system->dir_has_content(System::DATA_OUT_PATH) ==    true)
            {
                $this->get_data(System::DATA_OUT_PATH);
            }
        }
    }

    protected function get_data(string $dir)
    {
        $this->set_sysou_file_data($dir);
//        $this->system->print_pre($this->get_sysou_file_data());exit;
        return json_encode($this->get_sysou_file_data());exit;
    }

    private function set_sysou_file_data(string $dir)
    {
        if(file_exists($dir.'result.txt'))
        {
            $linecount  =   0;
            $handle     =   fopen($dir.'result.txt', "r");
            
            while(!feof($handle))
            {
                $linecount++;
                
                $line = fgets($handle);
              
                if(strpos($line, '@HL@'))
                {
                    $result = preg_replace("/[^A-Z]+/", "", $line);
                    $result = str_replace('GHL', '', $result);
                    
                    $this->form_type    =   $result[0];
                    $this->edition      =   $result[1];
                    
                    $key    =   $linecount;
                    $group  =   str_replace('','',substr($line,1,3));
                }
                
                if(!strpos($line, '@HL@'))
                {
                    $txt[$group][$key][]  =   $line;
                }
            }

            fclose($handle);

            if(array_key_exists('09', $txt))
            {
                $this->read_09($txt['09'],'infli09');
            }
            
            if(array_key_exists('11', $txt))
            {
                $this->read_11($txt['11'],'infli11');
            }
            
            if(array_key_exists('12', $txt))
            {
                $this->read_12($txt['12'],'infli12');
            }
            
            if(array_key_exists('44', $txt))
            {
                $this->read_44($txt['44'],'infli44');
            }
            
            if(array_key_exists('46', $txt))
            {
                $this->read_44($txt['46'],'infli46');
            }
            
            
            //exit('do 46, find 12');
        }
    } 
    
    protected function get_sysou_file_data()
    {
        return $this->blocks;
    }
    
    private function read_09(array $block_array,string $array_block_nr)
    {
        foreach ($block_array as $ka    =>  $array)
        {
            if(preg_match('/INFORMEX/', $array[0]))
            {
                $this->blocks[$array_block_nr.'_print_form'][]  =   $this->read_09_print_form($array);
            }
            else
            {
                $i=0;

                foreach ($array as $key   =>  $line)
                {
                    $line       =   ltrim($line,'');
                    $Blok_key   =   intval(substr($line, 0,2));   
                    $line       =   substr($line,2);

                    if(array_key_exists($Blok_key, $this->Bloknummer_09))
                    {
                        if(array_key_exists('exception', $this->Bloknummer_09[$Blok_key]))
                        {
                            $Nblock  =   $this->handle_exception($Blok_key,$line,$this->Bloknummer_09[$Blok_key]);

                            foreach ($Nblock as $b_key    =>  $block)
                            {
                                $line_data[$ka][$Blok_key][$b_key]   = trim($this->return_string_value($line,$block,1));
                            }
                        }
                        else
                        {
                            if($Blok_key    ==  '41')
                            {
                                $i++;

                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$i][$b_key]   = trim($this->return_string_value($line,$block));
                                } 
                                
                            }
                            else if($Blok_key   ==  '42')
                            {
                                $i++;

                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$i][$b_key]   = trim($this->return_string_value($line,$block));
                                } 
                            }
                            else if($Blok_key   ==  '57')
                            {
                                $i++;

                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$i][$b_key]   = trim($this->return_string_value($line,$block));
                                } 
                            }
                            else if($Blok_key   ==  '18')
                            {
                                $i++;

                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$i][$b_key]   = trim($this->return_string_value($line,$block));
                                } 
                            }
                            else if($Blok_key   ==  '19')
                            {
                                $i++;

                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$i][$b_key]   = trim($this->return_string_value($line,$block));
                                } 
                            }
                            else if($Blok_key   ==  '62')
                            {
                                $i++;

                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$i][$b_key]   = trim($this->return_string_value($line,$block));
                                } 
                            }
                            else
                            {
                                foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$ka][$Blok_key][$b_key]   = trim($this->return_string_value($line,$block));
                                }
                            }
                        }
                    }
                }

                $this->blocks[$array_block_nr][]   =   $line_data;
            }
        }
    }

    
    private function read_09_print_form(array $array)
    {
        $_print_form_bestek =   array();
		$_print_form_bestek['text_print'] = implode('',$array);
        
        /*  
         * entity type 
         */
        $_print_form_bestek['entity_type']        = trim(preg_replace('/^.*?('.self::ENTITY_TYPES.').*?$/si', '$1', $array[0]));
        
        /*  
         * informex number 
         */   
        $_print_form_bestek['informex_number']    =   trim(substr($array[1], 0,9));
        
        /*
         * @entity_type_number 
         */
        $search_string      =   str_replace($_print_form_bestek['informex_number'], '', $array[1]);
        $new_string_array   =   preg_split('/[\s,]+/', $search_string, 3);
        $_print_form_bestek['entity_type_number'] =   trim($new_string_array[1]);

        /*
         * iban
         */
        $_print_form_bestek['iban']               = trim(str_replace('WAN ','',trim($array[2])));

        /*
         * type of doc
         */
        $_print_form_bestek['doc_type']   =   trim(preg_replace('/\s+/', '', $array[3]));
        
        /*
         * date of doc
         */
        $_print_form_bestek['doc_date']   =   trim(preg_replace('/\s+/', '', $array[5]));
        
        
        #*****************************************************************************************************
        /*
         * vehicle owner
         */
        if(preg_match("~[0-9]~", $array[6])  ==  true)
        {
            $reference_nr   = substr($array[6], -8);
            
            $start  = str_replace('VOERTUIG VAN : ', '', $array[6]);
            
            $start  = preg_replace('/\d+/u', '', $start);
            
            if(strpos($start, '/'))
            {
                $_print_form_bestek['vehicle_owner'] = trim(substr($start, 0, strpos($start, "/")));
            }
            else
            {
                $reference_nr_alph  =   preg_replace('/[^a-zA-Z]+/', '', $reference_nr);
                $_print_form_bestek['vehicle_owner']      =   trim(str_replace($reference_nr_alph, '', $start));
            }
        }
        else
        {
            $_print_form_bestek['vehicle_owner']  = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $array[6] )));
        }        

        /*
         * vehicle owner telephone
         */
        if(preg_match("~[0-9]~", $array[6])  ==  true)
        {
            $reference_nr       =   substr($array[6], -8);
            $vehicle_owner_tel  =   str_replace($reference_nr, '', $array[6]);
            $_print_form_bestek['vehicle_owner_tel']  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $vehicle_owner_tel ));
        }

        /*
         * vehicle owner reference number
         */
        if(preg_match("~[0-9]~", $array[6])  ==  true)
        {
            $_print_form_bestek['reference_nr']       =   trim(substr($array[6], -8));
        }
        #*****************************************************************************************************
        /*
         * repairer
         */
        if(preg_match("~[0-9]~", $array[7])  ==  true)
        {
            $start  = str_replace('HERSTELLER   : ', '', $array[7]);
            $start  = preg_replace('/\d+/u', '', $start);
            
            if(strpos($start, '/'))
            {
                $repairer = trim(substr($start, 0, strpos($start, "/")));
            }
        }
        else
        {
            $start      = str_replace('HERSTELLER   : ', '', $array[7]);
            $_print_form_bestek['repairer']   = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
        }
        
        /*
         * repairer phone
         */
        if(preg_match("~[0-9]~", $array[7])  ==  true)
        {
            $_print_form_bestek['repairer_tel']  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[7] ));
            $_print_form_bestek['repairer_tel']     = str_replace('..', '', $_print_form_bestek['repairer_tel']);
            $_print_form_bestek['repairer_tel']  =   trim(preg_replace('/\s+/', '', $_print_form_bestek['repairer_tel'] ));
        }
        #*********************************************************************************************************
        
        for($i=9;$i<=20;$i++)
        {
            //array
            if(preg_match('/MAKELAAR/', $array[$i]))
            {
                /*
                 * broker
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $start  = trim(str_replace('MAKELAAR   : ', '', $array[$i]));
                    $start  = trim(str_replace('MAKELAAR', '', $start));
                    $start  = preg_replace('/\d+/u', '', $start);

                    if(strpos($start, '/'))
                    {
                        $_print_form_bestek['broker']   =   substr($start, 0, strpos($start, "/"));
                        $_print_form_bestek['broker']   =   str_replace(':', '', $_print_form_bestek['broker']);
                        $_print_form_bestek['broker']   =   trim(preg_replace('/\s+/', '', $_print_form_bestek['broker'] ));
                    }
                }
                else
                {
                    $start      = str_replace('MAKELAAR   : ', '', $array[$i]);
                    $_print_form_bestek['broker']    = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
                }

                /*
                 * broker_tel phone
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $_print_form_bestek['broker_tel']  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[$i] ));
                }
            }
            
            if(preg_match('/DESKUNDIGE/', $array[$i]))
            {
                /*
                 * expert
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $start  = str_replace('DESKUNDIGE   : ', '', $array[$i]);
                    $start  = preg_replace('/\d+/u', '', $start);

                    if(strpos($start, '/'))
                    {
                        $_print_form_bestek['expert'] = trim(substr($start, 0, strpos($start, "/")));
                    }
                }
                else
                {
                    $start      = str_replace('DESKUNDIGE   : ', '', $array[$i]);
                    $_print_form_bestek['expert']    = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
                }

                /*
                 * broker_tel phone
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $broker_tel  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[$i] ));
                }
            }
            
            if(preg_match('/MAATSCHAPPIJ/', $array[$i]))
            {
                /*
                 * insurer
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $start  = str_replace('MAATSCHAPPIJ   : ', '', $array[$i]);
                    $start  = preg_replace('/\d+/u', '', $start);

                    if(strpos($start, '/'))
                    {
                        $_print_form_bestek['insurance'] = trim(substr($start, 0, strpos($start, "/")));
                    }
                }
                else
                {
                    $start      = trim(str_replace('MAATSCHAPPIJ   : ', '', $array[$i]));
                    $start      = trim(str_replace('MAATSCHAPPIJ', '', $start));
                    $_print_form_bestek['insurance']     = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
                }

                /*
                 * broker_tel phone
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $_print_form_bestek['insurance_tel']  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[$i] ));
                }
            }

            
            if(strlen($array[$i]) <= 3)
            {
                $next_row_key   =   $i+1;
                break;
            }
        }
        
        #*********************************************************************************************************

        $_print_form_bestek['car_mark']         =   trim(substr($array[$next_row_key], 0,23));
        $_print_form_bestek['car_model']        =   trim(substr($array[$next_row_key], 24,23));
        $_print_form_bestek['car_sub_model']    =   trim(substr($array[$next_row_key], 48,14));
        $_print_form_bestek['car_something']    =   trim(substr($array[$next_row_key], 68,7));
        
        #***********************************************************************************************************
        
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if($i > $next_row_key && strlen($array[$i]) > 3)
            {
                $next_row_key   =   $i;
                break;
            }
        }

        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if(strlen($array[$i]) > 3)
            {
                if(preg_match('/REF/', $array[$i]))
                {
                    $_print_form_bestek['details_2'][]   =   array(
                        'REF_NR'    =>  trim(substr($array[$i+1], 0,9)),
                        'VERVANGEN_ONDERDELEN'  =>  trim(substr($array[$i+1], 13,20)),
                        'ONDERDEELNR'   =>  trim(substr($array[$i+1], 34,20)),
                        'PRIJS_AFTR_BTW'    =>  trim(substr($array[$i+1], 55,10)),
                        'AE'    =>  trim(substr($array[$i+1], 74,2)),
                    );
                }
                else
                {
                    $_print_form_bestek['details_1'][]   =   array(
                        'col_1'    =>  trim(substr($array[$i], 0,24)),
                        'col_2'  =>  trim(substr($array[$i], 24,24)),
                        'col_3'   =>  trim(substr($array[$i], 48,24)),
                    );
                }
            }
            else
            {
                $next_row_key   =   $i;
                break;
            }   
        }
            
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if($i > $next_row_key && strlen($array[$i]) > 3 && is_numeric(substr($array[$i], 0,1)))
            {
                $next_row_key   =   $i;
                break;
            }
        }

        #***********************************************************************************************************
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if(strlen($array[$i]) > 3)
            {
                $_print_form_bestek['details_3'][]   =   array(
                'REF_NR'    =>  trim(substr($array[$i], 0,9)),
                'VERVANGEN_ONDERDELEN'  =>  trim(substr($array[$i], 13,20)),
                'ONDERDEELNR'   =>  trim(substr($array[$i], 34,20)),
                'PRIJS_AFTR_BTW'    =>  trim(substr($array[$i], 55,10)),
                'AE'    =>  trim(substr($array[$i], 74,2)),
                );
            }
            else
            {
                $next_row_key   =   $i;
                break;
            }
        }
        
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if($i > $next_row_key && strlen($array[$i]) > 3 && preg_match('/REF/', $array[$i]))
            {
                $next_row_key_sm   =   $i;
                break;
            }
        }
        for($i=$next_row_key_sm;$i<=count($array);$i++)
        {
            if($i > $next_row_key_sm && strlen($array[$i]) > 3)
            {
                $next_row_key   =   $i;
                break;
            }
        }

        #***********************************************************************************************************  
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if(!preg_match('/REF/', $array[$i]) && strlen($array[$i]) > 3)
            {
                    $_print_form_bestek['BEWERKINGEN'][]   =   array(
                    'REF_NR'    =>  trim(substr($array[$i], 0,12)),
                    'VERVANGEN_ONDERDELEN'  =>  trim(substr($array[$i], 13,40)),
                    'PRIJS_AFTR_BTW'    =>  trim(substr($array[$i], 55,10)),
                    'AE'    =>  trim(substr($array[$i], 74,2)),
                    );
            }
            else
            {
                $next_row_key   =   $i;
                break;
            }
        }
        
        $arr_key_string =   '';
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if($i > $next_row_key && strlen($array[$i]) > 3 && preg_match('/SPUITEN/', $array[$i]))
            {
                $arr_key_string                =   'SPUITEN';
                $next_row_key_sm    =   $i;
                break;
            }
            else
            {
                if($i > $next_row_key && strlen($array[$i]) > 3)
                {
                    $next_row_key   =   $i;
                    break;
                }
            }
        }
        
        if($next_row_key_sm)
        {
            for($i=$next_row_key_sm;$i<=count($array);$i++)
            {
                if($i > $next_row_key_sm && strlen($array[$i]) > 3)
                {
                    $next_row_key   =   $i;
                    break;
                }
            }
        }

        #***********************************************************************************************************  
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if(strlen($array[$i]) > 3)
            {
                if($arr_key_string)
                {
                    $_print_form_bestek[$arr_key_string][]   =   array(
                    'REF_NR'    =>  trim(substr($array[$i], 0,12)),
                    'VERVANGEN_ONDERDELEN'  =>  trim(substr($array[$i], 13,40)),
                    'PRIJS_AFTR_BTW'    =>  trim(substr($array[$i], 55,10)),
                    'AE'    =>  trim(substr($array[$i], 74,2)),
                    );
                }
                else
                {
                    $_print_form_bestek['other'][]   =   array(
                    'REF_NR'    =>  trim(substr($array[$i], 0,12)),
                    'VERVANGEN_ONDERDELEN'  =>  trim(substr($array[$i], 13,40)),
                    'PRIJS_AFTR_BTW'    =>  trim(substr($array[$i], 55,10)),
                    'AE'    =>  trim(substr($array[$i], 74,2)),
                    );
                }
            }
            else
            {
                $next_row_key   =   $i;
                break;
            }
        }
        
        $eindberekening =   '';
        
        for($i=$next_row_key;$i<=count($array);$i++)
        {
            if($i > $next_row_key && strlen($array[$i]) > 3 && preg_match('/E I N D B E R E K E N I N G/', $array[$i]))
            {
                $eindberekening =   1;
                $next_row_key   =   $i;
                break;
            }
        }

        if($eindberekening && !empty($eindberekening))
        {
            for($i=$next_row_key;$i<=count($array);$i++)
            {
                if(!strpos($array[$i], 'INFORMEX'))
                {
                    $prim_eindberekening_array[]  =   $array[$i];
                }
                else
                {
                    $next_row_key   =   $i;
                    break;
                }
            }

            foreach ($prim_eindberekening_array as $key=>$row)
            {
                if(preg_match('/ONDERDELEN/', $row) && !preg_match('/TOTAAL ONDERDELEN/', $row))
                {
                    $eindberekening_array['onderdelen'] =   preg_replace('/[^0-9\,]*/i', '', $row);
                }
                
                if(preg_match('/TOTAAL ONDERDELEN/', $row))
                {
                    $eindberekening_array['total_onderdelen'] =   preg_replace('/[^0-9\,]*/i', '', $row);
                }
                
                if(preg_match('/LOONKOSTEN/', $row) && !preg_match('/TOTAAL LOONKOSTEN/', $row))
                {   
                    $small_string   =   trim(str_replace('LOONKOSTEN', '', $row));
                    $string_end     =   substr($small_string, -2);
                    $string_start   =   explode('=',str_replace($string_end, '', $small_string));

                    $working_hours  =   array(
                        'hour'          =>  intval(trim(str_replace('U.','',$string_start[0]))),
                        'amount'        =>  intval(trim($string_start[1])),
                        'type'          =>  $string_end,
                        );
                    
                    $eindberekening_array['loonkosten'] =   $working_hours;
                }
                
                if(strpos($row, 'UURLOON *'))
                {   
                    $small_string   =   trim(str_replace(' UURLOON *', '', $row));

                    preg_match_all('/\d+[\d,]*\s*/', $small_string,$nums);
                    
                    $new_string =   trim(str_replace(substr($small_string, strpos($small_string, ".")),'',$small_string));  
                    $time       =   trim(substr($new_string, strpos($new_string, "/")+1));    
                    $type       =   str_replace($time,'',preg_replace('/[^a-zA-Z]+/', '', $small_string));

                    $uurloon  =   array(
                        'time'          =>  $time,
                        'amount'        =>  trim($nums[0][0]),
                        'hours'         =>  intval(trim($nums[0][1])),
                        'total'         =>  trim($nums[0][2]),
                        'type'          =>  $type,
                        );
                    
                    $eindberekening_array['uurloon_1']  =   $uurloon;
                }
                
                if(preg_match('/TOTAAL LOONKOSTEN/', $row))
                {   
                    $keys[] =   $key;
                    preg_match('/\d+[\d,]*\s*/', $row,$total_loonkosten);
                    $eindberekening_array['totaal_loonkosten']  =   $total_loonkosten[0];
                }
                
                if(preg_match('/SPUITEN/', $row) && !preg_match('/TOTAAL SPUITEN/', $row))
                {   
                    $small_string   =   trim(str_replace('SPUITEN', '', $row));
                    $string_end     =   substr($small_string, -2);
                    $string_start   =   explode('=',str_replace($string_end, '', $small_string));

                    $spuiten_working_hours  =   array(
                        'hour'          =>  intval(trim(str_replace('U.','',$string_start[0]))),
                        'amount'        =>  intval(trim($string_start[1])),
                        'type'          =>  $string_end,
                        );
                    
                    $eindberekening_array['spuiten'] =   $spuiten_working_hours;
                }
                
                if(preg_match('/UURLOON/', $row))
                {   
                    $small_string   =   trim(str_replace(' UURLOON', '', $row));

                    preg_match_all('/\d+[\d,]*\s*/', $small_string,$nums);
                    
                    $new_string =   trim(str_replace(substr($small_string, strpos($small_string, ".")),'',$small_string));  
                    $time       =   trim(substr($new_string, strpos($new_string, "/")+1));    
                    $type       =   str_replace($time,'',preg_replace('/[^a-zA-Z]+/', '', $small_string));

                    $uurloon_2  =   array(
                        'time'          =>  $time,
                        'amount'        =>  trim($nums[0][0]),
                        'hours'         =>  intval(trim($nums[0][1])),
                        'total'         =>  trim($nums[0][2]),
                        'type'          =>  $type,
                        );
                    
                    $eindberekening_array['uurloon_2']  =   $uurloon_2;
                }
                
                if(preg_match('/TOTAAL SPUITEN/', $row))
                {   
                    preg_match('/\d+[\d,]*\s*/', $row,$total_spuiten);
                    $eindberekening_array['totaal_spuiten']  =   $total_spuiten[0];
                }
                    
                if(!preg_match('/ONDERDELEN/', $row)  && !preg_match('/LOONKOSTEN/', $row) && !preg_match('/UURLOON/', $row) && !preg_match('/SPUITEN/', $row) && !preg_match('/E I N D B E R E K E N I N G/', $row) && strlen($row) > 3)
                {
                    $txt =  preg_replace('/[^a-zA-Z]+\s\s+/', '', $row);
                    
                    if(!empty($txt))
                    {
                        if(strpos($row,'%'))
                        {
                            $txt    =   str_replace(array(' ','-','. ','._'),'_',$txt);
                            $txt    =   str_replace(array('(',')'),'',$txt);

                            $percent = trim(substr($row, strpos($row, "%")-3,4));
                            
                            
                            preg_match('/\d+[\d.,]*\s*/', str_replace($percent, '', $row),$nums);
                            
                            $eindberekening_array[trim(strtolower($txt))]   =   array(
                                'btw'   =>  $percent,
                                'amount'    =>  trim($nums[0]),
                            );
                            
                            //var_dump($txt);exit;
                        }
                        elseif(preg_match('/TECHNISCHE CONTROLE/', $row))
                        {
                            $explode    =   explode(' ', $row);
                            
                            $eindberekening_array[strtolower($explode[0]).'_'.strtolower($explode[1])]  =   $explode[2];
                        }
                        else
                        {
                            preg_match('/\d+[\d.,]*\s*/', $row,$nums);
                            
                            $txt    =   str_replace(array(' ','-','. ','._'),'_',$txt);
                            $txt    =   str_replace(array('(',')'),'',$txt);
                        
                            $eindberekening_array[trim(strtolower($txt))]  =   trim($nums[0]);
                        }
                    }
                } 
            }
        }
        $_print_form_bestek['eindberekening']   =   $eindberekening_array;
  
        #********************************************************************************************************/
        //AUDATEX GEGEVENS SECTION
        foreach ($array as $key =>  $value)
        {
            if(preg_match('/AUDATEX GEGEVENS/', $value))
            {
                $start_key  =   $key;
            }
        }
        
        $audatex_array  =   array();
        
        for($i=$start_key;$i<count($array);$i++)
        {
            if(strlen($array[$i]) > 3)
            {
                $txt =  preg_replace('/[^a-zA-Z]+\s\s+/', '', $array[$i]);
                
                if(!empty($txt))
                {
                    $audatex_prim_array[]    =   $array[$i];   
                    
                    if(!preg_match('/AUDATEX GEGEVENS/', $array[$i]))
                    {
                        if(preg_match('/AUDATEX/', $array[$i]))
                        {
                            $audatex_array['name']      =   'L'.$this->get_string_between($array[$i], 'L', 'TYPE');
                            $audatex_array['TYPE']      =   $this->get_string_between($array[$i], 'TYPE', 'LONEN');
                            $audatex_array['LONEN']     =   $this->get_string_between($array[$i], 'LONEN', 'L');
                            $audatex_array['L']         =   substr($array[$i], strpos($array[$i], "L ") + (strlen($array[$i])-7) );
                        }
                        
                        if(preg_match('/UITVOERING/', $array[$i]))
                        {
                            $audatex_array['UITVOERING']    =   trim(str_replace('UITVOERING', '', $array[$i]));
                        }
                        
                        if(preg_match('/BEW.INDEX/', $array[$i]))
                        {
                            $arr_k  =   substr($array[$i-1], 0,20);
                            $s_key  =   $i;
                        }
                        else
                        {
                            $prices =   array();
                            
                            if(!empty($s_key))
                            {
                                if($i >= $s_key)
                                {                                    
                                    $arr_key    =   str_replace(array(' ','_._','-'),'_',trim($arr_k));

                                    preg_match_all('/\d+[\d,.]*\s*/', $array[$i],$nums);

                                    foreach ($nums[0] as $num)
                                    {
                                        if(strpos($num, ','))
                                        {
                                            $prices[$i][] =   $num;
                                        }
                                    }
                                    
                                    if(!is_array($array[$i]))
                                    {
                                        $b_index    =   '';
                                        
                                        if(is_array($prices))
                                        {
                                            foreach ($prices as $price)
                                            {
                                                $b_index    =   str_replace($price,'',$array[$i]);
                                            }
                                        }
                                        else
                                        {
                                            $b_index    =   str_replace($price,'',$array[$i]);
                                        }
                                        
                                        if($b_index && !empty($b_index))
                                        {
                                            if(is_string($b_index) && $b_index !== null )
                                            {
                                                if(!preg_match('/NIET-STANDAARD/', $b_index))
                                                {
                                                    if($arr_key ==  'O_NIET_STANDAARD_B')
                                                    {
                                                        $b_index    =   preg_split("/[\s,]+/", $b_index);

                                                        $audatex_array[$arr_key][]  =   array(
                                                            'BEW.INDEX'             =>  $b_index[1].' '.$b_index[2],
                                                            'AE'                    =>  $b_index[3],
                                                            'OMSCHRIJVING'          =>  $b_index[4],
                                                            'REFERENTIE'            =>  '',
                                                            'EUR'                   =>  $prices[$i][0],
                                                            'SLIJT'                 =>  '',
                                                            'BTW'                   =>  '',
                                                            'KL'                    =>  '',
                                                        );
                                                    }
                                                    else
                                                    {
                                                        $b_index    =   preg_split("/[\s,]+/", $b_index);
                                                        $b_index    =   array_filter($b_index);

                                                        $audatex_array[$arr_key][]  =   array(
                                                            'BEW.INDEX'             =>  $b_index[1].' '.$b_index[2],
                                                            'AE'                    =>  $b_index[3],
                                                            'EUR'                   =>  $prices[$i][0],
                                                            'EUR_percent'           =>  $prices[$i][1],
                                                            'SLIJT'                 =>  '',
                                                        );
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        $_print_form_bestek['AUDATEX_GEGEVENS'] =   $audatex_array;

        return $_print_form_bestek;
    }
    
    
    private function read_11(array $block_array,string $array_block_nr)
    {
        $data_array =   array();
        $orders     =   array();

        foreach ($block_array as $arr_key   =>  $array)
        {
            $data_array[$arr_key]['bemiddelaarsnummer_aanvrager'] =  trim(substr($array[0], 11,16)); 
            $data_array[$arr_key]['uitgavedatum']                 =  trim(substr($array[1], 0,8)); 

            foreach ($array as $key =>  $line)
            {
                if($key >= 7)
                {   
                    $data_array[$arr_key]['table_data'][]   =   array(
                        'bestek_nummer'                 =>  substr($array[$key],0,14),
                        'opdrachtnummer'                =>  substr($array[$key],14,11),
                        'naam_van_schadelijder'         =>  substr($array[$key],25,30),
                        'MIJ'                           =>  substr($array[$key],55,5),
                        'VOERTUIG'                      =>  substr($array[$key],60,20),
                        'PLAAT'                         =>  substr($array[$key],80,11),
                        'ONGEVAL_data'                  =>  substr($array[$key],93,10),
                        'EXPERTISE_data'                =>  substr($array[$key],103,10),
                        'datum_van_laatste_aanpassing'  =>  substr($array[$key],113,10),
                    );
                }
            }
        }
        
        $this->blocks[$array_block_nr]  =   $data_array;
    }

    private function read_44(array $block_array,string $array_block_nr)
    {
        $invoice    =   array();
        $go_key     =   0;
        
        foreach ($block_array as $arr_key   =>  $array)
        {
            $invoice[$arr_key]['Lijstnummer']                         =   trim(substr($array[0],7,2));
            $invoice[$arr_key]['Nummer_van_bladzijde']                =   trim(substr($array[0],77,4));
            $invoice[$arr_key]['Munteenheid_van_de_bemiddelaar']      =   trim(substr($array[1],71,3));                                                                                                                                                             
            $invoice[$arr_key]['Nr_van_de_factuur']                   =   trim(substr($array[11],11,7));   
            $invoice[$arr_key]['Datum']                               =   substr($array[12],11,8);   
            $invoice[$arr_key]['Klant_de_bemiddelaar_nummer']         =   substr($array[9],43,9);  
            $invoice[$arr_key]['Klant_de_bemiddelaar_Naam']           =   substr($array[10],45,30);
            $invoice[$arr_key]['Klant_de_bemiddelaar_Straat']         =   substr($array[11],45,30);
            $invoice[$arr_key]['Postcode']                            =   substr($array[12],45,4);
            $invoice[$arr_key]['gemeente']                            =   substr($array[12],50,25);
            $invoice[$arr_key]['Ondernemingsnummer']                  =   substr($array[13],52,15);
            
            foreach ($array as $key =>  $line)
            {
                if($key >= 18)
                {   
                    if(!strpos($line, 'BEDRAG EXCL') && !empty(trim(substr($line,0,8))))
                    {
                        $invoice[$arr_key]['table_data'][]   =   array(
                            'Datum'                     =>  substr($line,0,8),
                            'Tariefcode_Informex'       =>  substr($line,9,4),
                            'Aanwijzing'                =>  substr($line,14,31),
                            'Refertes'                  =>  substr($line,46,6),
                            'Hoeveelheid_AANTAL'        =>  substr($line,52,7),
                            'Bedrag_excl_BTW'           =>  substr($line,60,14),
                        );
                    }
                    
                    if(strpos($line, 'BEDRAG EXCL'))
                    {
                        $go_key =   $key;
                    }
                }
            }
                        
            $invoice[$arr_key]['Bedrag_excl_BTW_en_munteenheid']            =   substr($array[$go_key],61,17);
            $invoice[$arr_key]['BTW_aanslagvoet_bedrag_en_munteenheid']     =   substr($array[$go_key+1],59,19);
            $invoice[$arr_key]['Totaal_incl_BTW_en_munteenheid']            =   substr($array[$go_key+3],61,17);
        }
        
        $this->blocks[$array_block_nr]  =   $invoice;
    }

    private function return_string_value($string,$location,$exception=false)
    {
        return  substr($string, $location['position']-1,$location['length']);
    }
    
    private function handle_exception($key,$line,$block)
    {
        $Type   =   $this->return_string_value($line,$block['exception']['Type']);
        return $block['exception'][$Type];
    }
	
	private function read_46(array $block_array,string $array_block_nr)
    {
        foreach ($block_array as $key   =>  $array)
        {
            $rekening[$key]['afgesloten_maand']   =   date('m/y');
            
            preg_match("/\d{2}\/\d{2}/",$array[0],$date_month);
            
            if(is_array($date_month) && !empty($date_month[0]))
            {
                $rekening[$key]['afgesloten_maand']   =   trim($date_month[0]);
            }
            
            $rekening[$key]['Bladzijde']                          =  trim(substr($array[0], -4));
            $rekening[$key]['munteenheid_van_de_bemiddelaar']     =  trim(preg_replace("/[^a-zA-Z]/",'',$array[1]));
            
            $rekening[$key]['Uitgavedatum']   =   date('d/m/y');
            
            preg_match("/\d{2}\/\d{2}\/\d{2}/",$array[1],$date);

            if(is_array($date) && !empty($date[0]))
            {
                $rekening[$key]['Uitgavedatum']   =   trim($date[0]);
            }
            
            $rekening[$key]['titel']  =   'Rekening uittreksel';
           
            $address_2          =   preg_replace('/[^0-9a-zA-Z]+\s\s+/','',$array[4]);
            $address_2_array    =   explode(' ',$address_2)[0];

            $rekening[$key]['Bemiddelaar_naam']           =   trim(preg_replace('/[^a-zA-Z]+\s\s+/','',$array[2]));
            $rekening[$key]['Bemiddelaar_straat']         =   trim(preg_replace('/[^0-9a-zA-Z]+\s\s+/','',$array[3]));
            $rekening[$key]['Bemiddelaar_city_zip']       =   trim(explode(' ',$address_2)[0]);
            $rekening[$key]['Bemiddelaar_city']           =   trim(str_replace($rekening[$key]['Bemiddelaar_city_zip'], '', $array[4]));
            
            $rekening[$key]['identificatie']              =   array();
            
            $next_key   =   0;
            
            for($i=0;$i<count($array);$i++)
            {
                if(preg_match('/---/', $array[$i]) && !preg_match('/ ---/', $array[$i]))
                {
                    $rekening[$key]['identificatie'][]  =   array(
                        'Datum_bewerking'           =>  trim(substr($array[$i+1], 0,6)),
                        'Code_dagboek'              =>  trim(explode('/',substr($array[$i+1], 7,17))[0]),
                        'Nummer_bewerking'          =>  trim(explode('/',substr($array[$i+1], 7,17))[1]),
                        'Nummer_stuk informex'      =>  trim(explode('/',substr($array[$i+1], 7,17))[2]),
                        'Omschrijving'              =>  trim(substr($array[$i+1], 25,20)),
                        'stuk_nummer'               =>  trim(substr($array[$i+1], 46,7)),
                        'in_ons_voordele'           =>  trim(substr($array[$i+1], 60,11)),
                        'in_uw_voordele'            =>  trim(substr($array[$i+1], 72,11)),  
                    );
                }
                else if(preg_match('/---/', $array[$i]) && preg_match('/ ---/', $array[$i]) && $i > 1)
                {
                    $next_key   =   $i+1;
                    break;
                }
            }
            
            if(preg_match('/SALDO/', $array[$next_key]))
            {
                preg_match('/\d+[\d,]*\s*/', $array[$next_key],$nums);
                $rekening[$key]['SALDO']  =   trim($nums[0]);
            }
            
            for($i=$next_key+1;$i<count($array);$i++)
            {
                if(preg_match('/IBAN/', $array[$i]))
                {
                    $rekening[$key]['IBAN']   =   trim(explode('IBAN',$array[$i])[1]);
                }
                if(preg_match('/REFERENTIENR/', $array[$i]))
                {
                    preg_match('/\d+[\d \/]*\s*/',$array[$i],$m);
                    $rekening[$key]['REFERENTIENR']   =   trim($m[0]);
                }
            }
        }

        $this->blocks[$array_block_nr]  =   $rekening;
    }
	
	private function read_12(array $block_array,string $array_block_nr)
    {
        $orders =   array();
        
        foreach ($block_array as $arr_key   =>  $array)
        {
            preg_match("/\d{2}\/\d{2}/",$array[0],$date_month);
            preg_match("/\d{2}\/\d{2}\/\d{2}/",$array[1],$date);

            $orders[$arr_key]['OVERZICHT_OPDRACHTEN_DATE']  =   trim($date[0]);
            $orders[$arr_key]['OVERZICHT_OPDRACHTEN_MONTH'] =   trim($date_month[0]);
            $orders[$arr_key]['EXPERTISEBUREAU']            =   trim($this->get_string_between($array[0], 'EXPERTISEBUREAU', 'BLDZ'));
            $orders[$arr_key]['EXPERTISEBUREAU_NR']         =   trim($this->get_string_between($array[0], 'LIJST 12', 'OVERZICHT'));
            
            foreach ($array as $k   =>  $line)
            {
                if(strlen($line) > 3)
                {
                    if(preg_match('/OPDRACHT             DOSSIER/', $line))
                    {
                        $sk = $k;
                    }

                    if(isset($sk) && $k > $sk)
                    {
                        $na[$k] =   $line;
                    }
                    
                    if(preg_match('/DOOR OPDRACHTGEVER/', $line))
                    {
                        $keys[] = $k;
                    }
                }
            }

            $chunked_array  =   array();
            
            foreach ($keys as $key=>$val_key)
            {
                $list   =   list($firstKey) = array_keys($na); 
                $fk     =   min($list);

                for($i=$fk;$i<=$val_key;$i++)
                {
                    if(array_key_exists($i, $na))
                    {
                        $chunked_array[$key][] = $na[$i];
                        unset($na[$i]);
                    }
                }
                unset($keys[$key]);
            }
            
            $max_array_key  =   max(array_keys($chunked_array));
            unset($chunked_array[$max_array_key]);

            foreach ($chunked_array as $lkey =>  $order_array)
            {
                for($l=0;$l<count($order_array);$l++)
                {
                    $orders[$arr_key][$lkey]['GEBRUIKER_OPDRACHT']   =  trim(preg_replace('/[0-9]+/', '', $order_array[0]));
                    $orders[$arr_key][$lkey]['GEBRUIKER_DOSSIER']    =  trim(preg_replace('/[a-zA-Z)( ]+\s+/', '', $order_array[0]));

                    if(!preg_match('/---------/', $order_array[$l]) && $l > 0)
                    {
                        preg_match("/[\\d+|\\d+.\\d+]\\,\\d{0,2}/", $order_array[$l],$nums);

                        $orders[$arr_key][$lkey]['details'][$l]  =   array(
                            'VERSCHULDIGD_AAN_INFORMEX' =>  trim($nums[0]),
                            'code'                      =>  trim(str_replace($nums[0],'',$order_array[$l])),
                        );
                    }
                    
                    if(preg_match('/---------/', $order_array[$l]))
                    {
                        break;
                    }
                }
            }
        }
        
        $this->blocks[$array_block_nr]  =   $orders;
    }

}
