<?php

namespace Informex;

class Request extends System
{
    private $system;
    private $params;
    private $auth_request_string;
	public $strInformex;
	
	
	
	public $JAR_PARAMS = array(
		'-accessNr'             =>  '#01596',
		'-accessPass'           =>  'IF7079',
		'-intermediary'         =>  '03883',
	//    '-intermediary'         =>  '19998',
		'-password'             =>  'KNINP',
		'-environment'          =>  'test',
		'-agreement'            =>  'PM1',
		'-checkDigit'           =>  '999999',
		'-inputPath'            =>  System::DATA_IN_PATH,
		'-outputPath'           =>  System::DATA_OUT_PATH,
	);
	
	public $JAR_PARAMS_IMG = array(
		'-accessNr'             =>  '#02312',
		'-accessPass'           =>  'IF7889',
		'-intermediary'         =>  '475869429',
	//    '-intermediary'         =>  '19998',
		'-password'             =>  'KNINP',
		'-environment'          =>  'test',
		'-agreement'            =>  'PM1',
		'-checkDigit'           =>  '999999',
		'-inputPath'            =>  System::PICS_IN_PATH,
		'-outputPath'           =>  System::PICS_OUT_PATH,
	);
	
	
    public function __construct()
    {
        $this->system   =   new System();
    }

    private function set_params(int $other   =   0)
    {
        if($other    ==  1)
        {
            //pictures
            foreach($this->JAR_PARAMS_IMG as $key =>  $value)
            {
                $this->params.= ' '.escapeshellarg($key.'='.$value).' ';
            }
        }
        else
        {
            foreach($this->JAR_PARAMS as $key =>  $value)
            {
                $this->params.= ' '.escapeshellarg($key.'='.$value).' ';
            }
        }
    }


    private function get_params()
    {
        return  $this->params;
    }

     private function set_auth_request_string(int $index, $data=null, $key=null)
    {
        switch ($index)
        {
            case 1:
                $this->auth_request_string  =   '@A@000@K@00@05@050384@';
            break;
            case 2:
                $this->build    =   new Build($data, $this->strInformex);
//                print_r($this->build);exit; 
                if($this->build !== false)
                {
					//@A@504@K@241018@24@011018@23@23659874@Y1@0@Y2@0@26@500@4D@Y@7G@N@12@I@E4@2519855@3D@241018@22@235114444@25@28000@G2@1@59@N@27@I@O9@0@0G@12@42@TEST@G3@SEAT@G0@1LOL908@67@N@I9@0000@0A@N@7B@N@2A@A@3A@N@Q5@TEST@85@N@95@0@4B@N@94@1234@96@N@8A@0@97@N@1E@0@2F@X@3F@X@4F@X@X0@A@
//                    $this->auth_request_string  =   '@A@504@K@2519447@24@011018@23@SCHA123465987@Y1@0,00@Y2@0,00@26@0,00@4D@N@7G@N@12@E@E4@2519447@3D@241018@22@POL123456789@G2@1@59@N@27@E@O9@3000,00@0G@12@42@JOLIEN VAN GORP@44@RUBENSHEIDE 12@46@2950@47@KAPELLEN@50@497714268@2G@JOLIEN^GMAIL,COM@52@DOCENT@49@N@78@GMAN@56@NOORDERLAAN 32@80@2060@81@ANTWERPEN@G3@NISSAN@75@QASHQAI@G0@1LOL404@60@SJUH1236547888545@Y5@4502@77@55@63@1199@64@99@Y6@99@65@5@67@010918@I9@010918@0A@N@7B@N@68@A@1A@241018@2A@E@4I@25@3A@Y@Q5@DIT IS EEN TEST OM TE KIJKEN OF ALLES CORRECT DOORKOMT EN DE STRING IN VERS@R0@ CHILLENDE DELEN VAN MAX 75 CHARS GESPLITST WORDT,HET IS DAN OOK DE BEDOELI@R5@ NG DAT DEZE OP VERSCHILLENDE LIJNEN KOMEN MET EEN MAX VAN 4 LIJNEN MET E@S0@E N MAX VAN 300 CHARS, DUS DIT WIL ZEGGEN DAT WANNEER DE TEKST GROTER IS D@85@N@95@15,00@86@1@4B@N@F9@1@96@N@8A@0,00@97@N@1E@0,00@2F@X@3F@X@4F@X@X0@A@';
                    $this->auth_request_string  =   $this->build->get_update_string();
                }
            break;
        }
    }

    private function get_auth_request_string()
    {
        return $this->auth_request_string;
    }

    public function do_request(int $index, $data=null, $strInformex=null)
    {
		$this->strInformex = $strInformex;
        $this->set_params();
        $this->set_auth_request_string($index, $data);
		
        $this->system->write_auth_request_file($this->get_auth_request_string());

        if($this->check_file_exists(System::JAR_PATH)   ==  true)
        {
            $this->system->execute_external_jar(System::JAR_PATH.$this->get_params());
        }

        if($_FILES)
        {
            $this->send_pictures();
        }
    }
	
	public function send_pictures()
    {
        $this->set_params(1);
        
        //Let us say you wish to send 10 pictures for the general album of the mission 1234567.
        //You then have to create the folder: C:\informex\pics-in\01234567.004 
        //This folder has to contain the 10 jpeg files you wish to send.
        //upload images to the folder
        
        if($this->check_file_exists(System::JAR_PATH_IMG)   ==  true)
        {
            $this->system->execute_external_jar(JAR_PATH_IMG.$this->get_params());
        }
    }
}