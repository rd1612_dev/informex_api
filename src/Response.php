<?php

namespace Informex;


class Response
{
    private $system;
    private $parser;
    private $data;

    public function __construct()
    {
        $this->system   =   new System();
        $this->parser   =   new Parser();
    }

    public function get_response()
    {
        //check the xml files in data-out and the result.txt
        if($this->system->check_file_exists(System::DATA_OUT_PATH.'result.txt') ==  true)
        {
            $this->get_xml_data();
        }
    }

    private function get_xml_data(string $dir)
    {
        $this->data =   $this->parser->parse_xml($dir);
    }






}