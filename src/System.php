<?php

namespace Informex;

class System
{
	const JAR_PATH = __DOCROOT__.'/vendor/insypro/informex/lib/toolbox-data-0.1.5.jar';
	const DATA_IN_PATH = __DOCROOT__.'/vendor/insypro/informex/informex/data-in/';
	const DATA_OUT_PATH = __DOCROOT__.'/vendor/insypro/informex/informex/data-out/';
	
	const PICS_IN_PATH = __DOCROOT__.'/vendor/insypro/informex/informex/pics-in/';
	const PICS_OUT_PATH = __DOCROOT__.'/vendor/insypro/informex/informex/pics-out/';
	
	public function check_file_exists(string $dir)
    {
        if(file_exists($dir)   ==  false)
        {
            die('File missing');
        }
        else
        {
            return true;
        }
    }

    public function execute_external_jar(string $dir)
    {
        exec('java -jar '.$dir, $output);
    }

    public function write_auth_request_file(string $content)
    {
        //check if DATA_IN_PATH has read and write permissions
        if (!file_put_contents(self::DATA_IN_PATH.'data.txt', $content) !== false)
        {
            echo "Cannot create file";
        }
    }
    
    public function dir_has_content($dir) 
    {
        if (!is_readable($dir)) return NULL; 
        
            if((count(scandir($dir))) > 0)
            {
                return true;
            }
    }
    
    public function read_file(string $file): string
    {
        $contents   =   '';
        $handle     =   fopen($file, "rb");
        $contents   =   fread($handle, filesize($file));
        
        fclose($handle);
        
        return  $contents;
    }
    
    public function text_to_lines(string $file): array
    {
        $lines  =   array();
        
        $handle = fopen($file, "r");
        
        if ($handle) 
        {
            while (($line = fgets($handle)) !== false) 
            {
                  $lines[]  =     $line;
            }

            fclose($handle);
        }
        
        return $lines;
    }
    
    public function print_pre($data)
    {
        echo '<pre>';print_r($data);echo '</pre>';
    }
    
    public function get_string_between($str,$from,$to)
    {
        $sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
        return substr($sub,0,strpos($sub,$to));
    }
    
}