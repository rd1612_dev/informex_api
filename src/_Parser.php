<?php

namespace Informex;

class Parser extends System
{
    public $blocks  =   array();
    
    private $data;
    private $system;
    private $Bloknummer_09;
    private $form_type;
    private $edition;

    const ENTITY_TYPES  =   'OPDRACHTGEVER|HERSTELLER|DESKUNDIGE|MAKELAAR';

    public function __construct()
    {
        $this->system   =   new System();
            
        $this->Bloknummer_09           =   include_once ( __DOCROOT__.'/vendor/insypro/informex/lib/infli09.php');
//        $this->Bloknummer_44           =   include_once ( __DOCROOT__.'/vendor/insypro/informex/lib/infli44.php');
        
        if(is_dir(System::DATA_OUT_PATH))
        {
            if($this->system->dir_has_content(System::DATA_OUT_PATH) ==    true)
            {
                $this->get_data(System::DATA_OUT_PATH);
            }
        }
    }

    protected function get_data(string $dir)
    {
        $this->set_sysou_file_data($dir);
        $this->system->print_pre($this->get_sysou_file_data());exit;
        echo json_encode($this->get_sysou_file_data());exit;
    }

    private function set_sysou_file_data(string $dir)
    {
        if(file_exists($dir.'result.txt'))
        {
            $linecount  =   0;
            $handle     =   fopen($dir.'result.txt', "r");
            
            while(!feof($handle))
            {
                $linecount++;
                
                $line = fgets($handle);
              
                if(strpos($line, '@HL@'))
                {
                    $result = preg_replace("/[^A-Z]+/", "", $line);
                    $result = str_replace('GHL', '', $result);
                    
                    $this->form_type    =   $result[0];
                    $this->edition      =   $result[1];
                    
                    $key    =   $linecount;
                    $group  =   str_replace('','',substr($line,1,3));
                }
                
                if(!strpos($line, '@HL@'))
                {
                    $txt[$group][$key][]  =   $line;
                }
            }

            fclose($handle);

            if(array_key_exists('09', $txt))
            {
                $this->read_09($txt['09'],'infli09');
            }
            
            if(array_key_exists('11', $txt))
            {
                //$this->read_11($txt['11'],'infli11');
            }
            
            if(array_key_exists('12', $txt))
            {
                //$this->read_12($txt['12'],'infli12');
            }
            
            if(array_key_exists('44', $txt))
            {
                //$this->read_44($txt['44'],'infli44');
            }
            
            exit('do 46, find 12');
        }
    } 
    
    protected function get_sysou_file_data()
    {
        return $this->blocks;
    }
    
    private function read_09(array $block_array,string $array_block_nr)
    {
        foreach ($block_array as $array)
        {
            if(strpos($array[0], 'INFORMEX'))
            {
                //$this->read_09_print_form($array);
            }
            else
            {
                foreach ($array as $key   =>  $line)
                {
                    $line       =   ltrim($line,'');
                    $Blok_key   =   intval(substr($line, 0,2));   
                    $line       =   substr($line,2);

                    if(array_key_exists($Blok_key, $this->Bloknummer_09))
                    {
                        if(array_key_exists('exception', $this->Bloknummer_09[$Blok_key]))
                        {
                            $Nblock  =   $this->handle_exception($Blok_key,$line,$this->Bloknummer_09[$Blok_key]);

                            foreach ($Nblock as $b_key    =>  $block)
                            {
                                $line_data[$Blok_key][$b_key]   = $this->return_string_value($line,$block,1);
                            }
                        }
                        else
                        {
                            foreach ($this->Bloknummer_09[$Blok_key] as $b_key    =>  $block)
                            {
                                $line_data[$Blok_key][$b_key]   = $this->return_string_value($line,$block);
                            }
                        }
                    }
                }
            }

            empty($line_data[7]['inlichtingen_voertuig_Switch_location']) ? $line_data[7]['inlichtingen_voertuig_Switch'] =   'N' : true;
			print_r($line_data);exit;
            $this->blocks[$array_block_nr][$line_data[1]['opdrachtnummer']]['result']   =   $line_data;
        }
    }
    
    private function read_09_print_form(array $array)
    {
        //$this->print_pre($array[1]);

        /*  
         * entity type 
         */
        $entity_type        = preg_replace('/^.*?('.self::ENTITY_TYPES.').*?$/si', '$1', $array[0]);
        
        /*  
         * informex number 
         */   
        $informex_number    =   substr($array[1], 0,9);
        
        /*
         * @entity_type_number 
         */
        $search_string      =   str_replace($informex_number, '', $array[1]);
        $new_string_array   =   preg_split('/[\s,]+/', $search_string, 3);
        $entity_type_number =   trim($new_string_array[1]);

        /*
         * iban
         */
        $iban               = str_replace('WAN ','',trim($array[2]));

        /*
         * type of doc
         */
        $doc_type   =   preg_replace('/\s+/', '', $array[3]);
        
        /*
         * date of doc
         */
        $doc_date   =   preg_replace('/\s+/', '', $array[5]);
        
        
        #*****************************************************************************************************
        /*
         * vehicle owner
         */
        if(preg_match("~[0-9]~", $array[6])  ==  true)
        {
            $reference_nr   = substr($array[6], -8);
            
            $start  = str_replace('VOERTUIG VAN : ', '', $array[6]);
            
            $start  = preg_replace('/\d+/u', '', $start);
            
            if(strpos($start, '/'))
            {
                $vehicle_owner = trim(substr($start, 0, strpos($start, "/")));
            }
            else
            {
                $reference_nr_alph  =   preg_replace('/[^a-zA-Z]+/', '', $reference_nr);
                $vehicle_owner      =   trim(str_replace($reference_nr_alph, '', $start));
            }
        }
        else
        {
            $vehicle_owner  = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $array[6] )));
        }        

        /*
         * vehicle owner telephone
         */
        if(preg_match("~[0-9]~", $array[6])  ==  true)
        {
            $reference_nr       =   substr($array[6], -8);
            $vehicle_owner_tel  =   str_replace($reference_nr, '', $array[6]);
            $vehicle_owner_tel  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $vehicle_owner_tel ));
        }

        /*
         * vehicle owner reference number
         */
        if(preg_match("~[0-9]~", $array[6])  ==  true)
        {
            $reference_nr       =   substr($array[6], -8);
        }
        #*****************************************************************************************************
        /*
         * repairer
         */
        if(preg_match("~[0-9]~", $array[7])  ==  true)
        {
            $start  = str_replace('HERSTELLER   : ', '', $array[7]);
            $start  = preg_replace('/\d+/u', '', $start);
            
            if(strpos($start, '/'))
            {
                $repairer = trim(substr($start, 0, strpos($start, "/")));
            }
        }
        else
        {
            $start      = str_replace('HERSTELLER   : ', '', $array[7]);
            $repairer   = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
        }
        
        /*
         * repairer phone
         */
        if(preg_match("~[0-9]~", $array[7])  ==  true)
        {
            $repairer_tel  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[7] ));
        }
        #*********************************************************************************************************

        for($i=9;$i<=11;$i++)
        {
            if(strpos($array[$i], 'MAKELAAR'))
            {
                /*
                 * broker
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $start  = str_replace('MAKELAAR   : ', '', $array[$i]);
                    $start  = preg_replace('/\d+/u', '', $start);

                    if(strpos($start, '/'))
                    {
                        $broker = trim(substr($start, 0, strpos($start, "/")));
                    }
                }
                else
                {
                    $start      = str_replace('MAKELAAR   : ', '', $array[$i]);
                    $broker    = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
                }

                /*
                 * broker_tel phone
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $broker_tel  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[$i] ));
                }
            }
            
            if(strpos($array[$i], 'DESKUNDIGE'))
            {
                /*
                 * broker
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $start  = str_replace('DESKUNDIGE   : ', '', $array[$i]);
                    $start  = preg_replace('/\d+/u', '', $start);

                    if(strpos($start, '/'))
                    {
                        $broker = trim(substr($start, 0, strpos($start, "/")));
                    }
                }
                else
                {
                    $start      = str_replace('DESKUNDIGE   : ', '', $array[$i]);
                    $broker    = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
                }

                /*
                 * broker_tel phone
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $broker_tel  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[$i] ));
                }
            }
            
            if(strpos($array[$i], 'MAATSCHAPPIJ'))
            {
                /*
                 * insurer
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $start  = str_replace('MAATSCHAPPIJ   : ', '', $array[$i]);
                    $start  = preg_replace('/\d+/u', '', $start);

                    if(strpos($start, '/'))
                    {
                        $broker = trim(substr($start, 0, strpos($start, "/")));
                    }
                }
                else
                {
                    $start      = str_replace('MAATSCHAPPIJ   : ', '', $array[$i]);
                    $broker     = trim(str_replace(':','',preg_replace("/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/", '', $start )));
                }

                /*
                 * broker_tel phone
                 */
                if(preg_match("~[0-9]~", $array[$i])  ==  true)
                {
                    $broker_tel  =   trim(preg_replace('/[^0-9 \/ \.]/', '', $array[$i] ));
                }
            }
            
            if(strlen($array[$i]) <= 3)
            {
                $next_row_key   =   $i+1;
            }
        }
        #*********************************************************************************************************
        
        
        
        var_dump($array[$next_row_key]);exit('qqq');


                    
        exit;
    }
    
    
    private function read_11(array $block_array,string $array_block_nr)
    {
        $data_array =   array();
        $orders     =   array();

        foreach ($block_array as $arr_key   =>  $array)
        {
            $data_array[$arr_key]['bemiddelaarsnummer_aanvrager'] =  trim(substr($array[0], 11,16)); 
            $data_array[$arr_key]['uitgavedatum']                 =  trim(substr($array[1], 0,8)); 

            foreach ($array as $key =>  $line)
            {
                if($key >= 7)
                {   
                    $data_array[$arr_key]['table_data'][]   =   array(
                        'bestek_nummer'                 =>  substr($array[$key],0,14),
                        'opdrachtnummer'                =>  substr($array[$key],14,11),
                        'naam_van_schadelijder'         =>  substr($array[$key],25,30),
                        'MIJ'                           =>  substr($array[$key],55,5),
                        'VOERTUIG'                      =>  substr($array[$key],60,20),
                        'PLAAT'                         =>  substr($array[$key],80,11),
                        'ONGEVAL_data'                  =>  substr($array[$key],93,10),
                        'EXPERTISE_data'                =>  substr($array[$key],103,10),
                        'datum_van_laatste_aanpassing'  =>  substr($array[$key],113,10),
                    );
                }
            }
        }
        
        $this->blocks[$array_block_nr]  =   $data_array;
    }

    private function read_44(array $block_array,string $array_block_nr)
    {
        $invoice    =   array();
        $go_key     =   0;
        
        foreach ($block_array as $arr_key   =>  $array)
        {
            $invoice[$arr_key]['Lijstnummer']                         =   trim(substr($array[0],7,2));
            $invoice[$arr_key]['Nummer_van_bladzijde']                =   trim(substr($array[0],77,4));
            $invoice[$arr_key]['Munteenheid_van_de_bemiddelaar']      =   trim(substr($array[1],71,3));                                                                                                                                                             
            $invoice[$arr_key]['Nr_van_de_factuur']                   =   trim(substr($array[11],11,7));   
            $invoice[$arr_key]['Datum']                               =   substr($array[12],11,8);   
            $invoice[$arr_key]['Klant_de_bemiddelaar_nummer']         =   substr($array[9],43,9);  
            $invoice[$arr_key]['Klant_de_bemiddelaar_Naam']           =   substr($array[10],45,30);
            $invoice[$arr_key]['Klant_de_bemiddelaar_Straat']         =   substr($array[11],45,30);
            $invoice[$arr_key]['Postcode']                            =   substr($array[12],45,4);
            $invoice[$arr_key]['gemeente']                            =   substr($array[12],50,25);
            $invoice[$arr_key]['Ondernemingsnummer']                  =   substr($array[13],52,15);
            
            foreach ($array as $key =>  $line)
            {
                if($key >= 18)
                {   
                    if(!strpos($line, 'BEDRAG EXCL') && !empty(trim(substr($line,0,8))))
                    {
                        $invoice[$arr_key]['table_data'][]   =   array(
                            'Datum'                     =>  substr($line,0,8),
                            'Tariefcode Informex'       =>  substr($line,9,4),
                            'Aanwijzing'                =>  substr($line,14,31),
                            'Refertes'                  =>  substr($line,46,6),
                            'Hoeveelheid_AANTAL'        =>  substr($line,52,7),
                            'Bedrag_excl_BTW'           =>  substr($line,60,14),
                        );
                    }
                    
                    if(strpos($line, 'BEDRAG EXCL'))
                    {
                        $go_key =   $key;
                    }
                }
            }
                        
            $invoice[$arr_key]['Bedrag_excl_BTW_en_munteenheid']            =   substr($array[$go_key],61,17);
            $invoice[$arr_key]['BTW_aanslagvoet_bedrag_en_munteenheid']     =   substr($array[$go_key+1],59,19);
            $invoice[$arr_key]['Totaal_incl_BTW_en_munteenheid']            =   substr($array[$go_key+3],61,17);
        }
        
        $this->blocks[$array_block_nr]  =   $invoice;
    }

    private function return_string_value($string,$location,$exception=false)
    {
        return  substr($string, $location['position']-1,$location['length']);
    }
    
    private function handle_exception($key,$line,$block)
    {
        $Type   =   $this->return_string_value($line,$block['exception']['Type']);
        return $block['exception'][$Type];
    }
}
