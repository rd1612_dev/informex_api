<?php

namespace Informex;

class Parser
{
	public $blocks  =   array();
    private $data;
    private $system;
    private $Bloknummer;
	
	const INFLI09 =  __DOCROOT__.'/vendor/insypro/informex/lib/infli09.php';
    
    public function __construct()
    {
        $this->system   =   new System();
                
        if(is_dir(System::DATA_OUT_PATH))
        {
            if($this->system->dir_has_content(System::DATA_OUT_PATH) ==    true)
            {
                $this->get_data(System::DATA_OUT_PATH);
            }
        }
    }

    protected function get_data(string $dir)
    {
        $this->set_sysou_file_data($dir);
//		echo "<pre>";
//		print_r($this->get_sysou_file_data());
        return json_encode($this->get_sysou_file_data());
    }
    
    protected function get_sysou_file_data()
    {
        return $this->blocks;
    }
    
    private function set_sysou_file_data(string $dir)
    {
        $main_blocks    =   array();
        
        if(file_exists($dir.'result.txt'))
        {
            $content                    =   $this->system->read_file($dir.'result.txt');
            $content_from_start_to_end  =   $this->system->get_string_between($content, '@HL@', '@FS@');
            $content_array              =   explode('@HL@', $content_from_start_to_end);
            $this->Bloknummer           =   include_once (self::INFLI09);
            //$this->data                 =   include_once ('lib/data.php');
            
            foreach ($content_array as $array)
            {
                $explode    =   explode("\n", $array);
                
                unset($explode[0]);

                if(count($explode) > 12)
                {
                    foreach ($explode as $key   =>  $line)
                    {
                        $line       =   ltrim($line,'');
                        $Blok_key   =   intval(substr($line, 0,2));   
                        $line       =   substr($line,2);

                        if(array_key_exists($Blok_key, $this->Bloknummer))
                        {
                            if(array_key_exists('exception', $this->Bloknummer[$Blok_key]))
                            {
                                $Nblock  =   $this->handle_exception($Blok_key,$line,$this->Bloknummer[$Blok_key]);

                                foreach ($Nblock as $b_key    =>  $block)
                                {
                                    $line_data[$Blok_key][$b_key]   = $this->return_string_value($line,$block,1);
                                }
                            }
                            else
                            {
                                foreach ($this->Bloknummer[$Blok_key] as $b_key    =>  $block)
                                {
                                    $line_data[$Blok_key][$b_key]   = $this->return_string_value($line,$block);
                                }
                            }
                        }
                    }

                    empty($line_data[7]['inlichtingen_voertuig_Switch_location']) ? $line_data[7]['inlichtingen_voertuig_Switch'] =   'N' : true;

                    $this->blocks[$line_data[1]['opdrachtnummer']]['result']   =   $line_data;
					
                }
            }
        }
    } 
    
    private function return_string_value($string,$location,$exception=false)
    {
        return  substr($string, $location['position']-1,$location['length']);
    }
    
    private function handle_exception($key,$line,$block)
    {
        $Type   =   $this->return_string_value($line,$block['exception']['Type']);
        return $block['exception'][$Type];
    }
}
